const HtmlWebPackPlugin = require('html-webpack-plugin');
const path = require('path');
const webpack = require('webpack');


const isProduction = process.env.NODE_ENV === 'production';

const config = {
    entry: './src/index.js',
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: '[name].[contenthash].bundle.js',
        chunkFilename: '[name].[contenthash].bundle.js',
        publicPath: '/',
    },
    resolve: {
        modules: [
            'node_modules',
            path.resolve(__dirname, 'src')
        ],
        extensions: ['.js', '.json', '.jsx', '.css'],
        alias: {
            Component: path.resolve(__dirname, 'src/components/'),
            Util: path.resolve(__dirname, 'src/utils/'),
            Constant: path.resolve(__dirname, 'src/constants/'),
            Reducer: path.resolve(__dirname, 'src/reducers/'),
            Store: path.resolve(__dirname, 'src/store/'),
            Action: path.resolve(__dirname, 'src/actions/'),
            Selector: path.resolve(__dirname, 'src/selectors/'),
            Section: path.resolve(__dirname, 'src/sections/'),
            Style: path.resolve(__dirname, 'src/assets/styles/'),
            Image: path.resolve(__dirname, 'src/assets/images/'),
        },
    },
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                loader: 'babel-loader',
            },
            {
                test: /\.html$/,
                use: ['html-loader']
            },
            {
                test: /\.jpe?g$|\.ico$|\.gif$|\.png$|\.svg$|\.woff$|\.ttf$|\.wav$|\.mp3$/,
                loader: 'file-loader?name=[name].[ext]'
            },
            {
                test: /\.css$/,
                use: ['style-loader', 'css-loader'],
            },
        ]
    },
    plugins: [
        new HtmlWebPackPlugin({
            template: './src/index.html',
            filename: './index.html'
        }),
        new webpack.DefinePlugin({
            API_URL: isProduction ? '"https://denlap.sevadenisov.com:3000/"' : '"https://localhost:3000/"',
            DEV_MODE: JSON.stringify(!isProduction)
        }),
        new webpack.HashedModuleIdsPlugin(),
    ],
    optimization: {
        runtimeChunk: 'single',
        splitChunks: {
            cacheGroups: {
                commons: {
                    chunks: 'initial',
                    minChunks: 2,
                    maxInitialRequests: 20, // The default limit is too small to showcase the effect
                    minSize: 400 // This is example is too small to create commons chunks
                },
                vendor: {
                    test: /node_modules/,
                    chunks: 'initial',
                    name: 'vendor',
                    priority: 10,
                    enforce: true
                }
            }
        },
        minimize: isProduction
    },
    devServer: {
        contentBase: path.join(__dirname, 'dist'),
        compress: true,
        port: 3011
    },
    devtool: isProduction ? 'source-map' : 'inline-source-map',
};


module.exports = config;
