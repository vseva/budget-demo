import { combineReducers } from 'redux';
import {
    SET_PERIOD_FILTER_END_DATE,
    SET_PERIOD_FILTER_START_DATE,
    SET_PERIOD_MAX_DATE,
    SET_PERIOD_MIN_DATE
} from 'Constant/actions';


const startDateReducer = (state = null, { type, payload }) => {
    if (type === SET_PERIOD_FILTER_START_DATE) return payload;

    return state;
};

const endDateReducer = (state = null, { type, payload }) => {
    if (type === SET_PERIOD_FILTER_END_DATE) return payload;

    return state;
};

export const minDateReducer = (state = null, { type, payload }) => {
    if (type === SET_PERIOD_MIN_DATE) return payload;

    return state;
};

export const maxDateReducer = (state = null, { type, payload }) => {
    if (type === SET_PERIOD_MAX_DATE) return payload;

    return state;
};


export default combineReducers({
    start: startDateReducer,
    end: endDateReducer,
    minDate: minDateReducer,
    maxDate: maxDateReducer,
});
