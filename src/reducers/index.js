import { combineReducers } from 'redux';

import data from 'Reducer/data';
import expensesChart from 'Reducer/expensesChart';
import incomeChart from 'Reducer/incomeChart';
import expensesCategoriesFilter from 'Reducer/expensesCategoriesFilter';
import incomeCategoriesFilter from 'Reducer/incomeCategoriesFilter';
import ui from 'Reducer/ui';
import periodFilter from 'Reducer/periodFilter';
import dayDetails from 'Reducer/dayDetails';
import categoryDetails from 'Reducer/categoryDetails';
import vendorDetails from 'Reducer/vendorDetails';
import newEntry from 'Reducer/newEntry';
import editEntry from 'Reducer/editEntry';


export default combineReducers({
    data,
    expensesChart,
    incomeChart,
    expensesCategoriesFilter,
    incomeCategoriesFilter,
    ui,
    periodFilter,
    dayDetails,
    newEntry,
    editEntry,
    categoryDetails,
    vendorDetails,
});
