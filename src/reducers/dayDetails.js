import { combineReducers } from 'redux';
import { CLOSE_DAY_DETAILS, OPEN_DAY_DETAILS, SET_DAY_DETAILS_DATE } from 'Constant/actions';


const dayDetailsOpenedReducer = (state = false, { type }) => {
    if (type === OPEN_DAY_DETAILS) return true;
    if (type === CLOSE_DAY_DETAILS) return false;

    return state;
};

const dayDetailsDateReducer = (state = null, { type, payload }) => {
    if (type === SET_DAY_DETAILS_DATE) return payload;

    return state;
};

export default combineReducers({
    opened: dayDetailsOpenedReducer,
    date: dayDetailsDateReducer
});
