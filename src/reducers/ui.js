import { combineReducers } from 'redux';

import {
    CLOSE_EXPENSES_CATEGORIES_FILTER, CLOSE_INCOME_CATEGORIES_FILTER,
    CLOSE_SIDE_MENU,
    OPEN_EXPENSES_CATEGORIES_FILTER, OPEN_INCOME_CATEGORIES_FILTER,
    OPEN_SIDE_MENU,
    SET_SITE_SECTION
} from 'Constant/actions';
import { SITE_SECTIONS } from 'Constant'



const currentSectionReducer = (state = SITE_SECTIONS[0].id, { type, payload }) => {
    if (type === SET_SITE_SECTION) {
        return payload;
    }

    return state;
};

const sideMenuReducer = (state = false, { type }) => {
    if (type === CLOSE_SIDE_MENU) return false;
    if (type === OPEN_SIDE_MENU) return true;

    return state;
};

const expensesCategoriesFilterOpenedReducer = (state = false, { type }) => {
    if (type === OPEN_EXPENSES_CATEGORIES_FILTER) return true;
    if (type === CLOSE_EXPENSES_CATEGORIES_FILTER) return false;

    return state;
};

const incomeCategoriesFilterOpenedReducer = (state = false, { type }) => {
    if (type === OPEN_INCOME_CATEGORIES_FILTER) return true;
    if (type === CLOSE_INCOME_CATEGORIES_FILTER) return false;

    return state;
};

export default combineReducers({
    currentSection: currentSectionReducer,
    sideMenu: sideMenuReducer,
    expensesCategoriesFilterOpened: expensesCategoriesFilterOpenedReducer,
    incomeCategoriesFilterOpened: incomeCategoriesFilterOpenedReducer,
});
