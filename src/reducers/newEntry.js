import { combineReducers } from 'redux';
import moment from 'moment';

import {
    NEW_ENTRY_FORM_FIELD_UPDATED,
    NEW_ENTRY_REQUEST_START,
    NEW_ENTRY_REQUEST_SUCCESS,
    CLOSE_NEW_ENTRY_MODAL,
    OPEN_NEW_ENTRY_MODAL,
    NEW_ENTRY_REQUEST_FAIL, SET_ENTRY_TYPE,
} from 'Constant/actions';
import { EXPENSE_ENTRY_TYPE, STORE_DATE_FORMAT } from 'Constant';


const modalOpenedReducer = (state = false, { type }) => {
    if (type === OPEN_NEW_ENTRY_MODAL) return true;
    if (type === CLOSE_NEW_ENTRY_MODAL) return false;
    if (type === NEW_ENTRY_REQUEST_SUCCESS) return false;

    return state;
};

const initialFormData = {
    date: moment().format(STORE_DATE_FORMAT),
};

const formDataReducer = (state = initialFormData, { type, payload }) => {
    if (type === NEW_ENTRY_FORM_FIELD_UPDATED) {
        const { name, value } = payload;

        return { ...state, [name]: value };
    }

    if (
        type === NEW_ENTRY_REQUEST_SUCCESS ||
        type === CLOSE_NEW_ENTRY_MODAL ||
        type === SET_ENTRY_TYPE
    ) {
        return initialFormData;
    }

    return state;
};

const entryTypeReducer = (state = EXPENSE_ENTRY_TYPE, { type, payload }) => {
    if (type === SET_ENTRY_TYPE) return payload;

    return state;
};

const requestInProgressReducer = (state = false, { type }) => {
    if (type === NEW_ENTRY_REQUEST_START) return true;
    if (
        type === NEW_ENTRY_REQUEST_SUCCESS ||
        type === NEW_ENTRY_REQUEST_FAIL
    ) {
        return false;
    }

    return state;
};


export default combineReducers({
    modalOpened: modalOpenedReducer,
    formData: formDataReducer,
    requestInProgress: requestInProgressReducer,
    entryType: entryTypeReducer,
});
