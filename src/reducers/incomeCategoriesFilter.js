import uniqBy from 'lodash/uniqBy';
import orderBy from 'lodash/orderBy';

import {
    CHECK_ALL_INCOME_CATEGORIES,
    REQUEST_INCOME_SUCCESS,
    TOGGLE_INCOME_CATEGORY,
    UNCHECK_ALL_INCOME_CATEGORIES,
} from 'Constant/actions';
import { getCategoryColor } from 'Util';


const incomeCategoriesReducer = (state = [], { type, payload }) => {
    if (type === REQUEST_INCOME_SUCCESS) {
        return orderBy(
            uniqBy(payload, 'category').map(({ category }) => ({
                checked: true,
                color: getCategoryColor(category),
                category,
            })),
            'category'
        );
    }

    if (type === TOGGLE_INCOME_CATEGORY) {
        const { category, checked } = payload;

        return state.map(item => {
            if (item.category === category) {
                return { ...item, checked };
            }

            return item;
        });
    }

    if (type === CHECK_ALL_INCOME_CATEGORIES) {
        return state.map(item => ({
            ...item,
            checked: true,
        }))
    }

    if (type === UNCHECK_ALL_INCOME_CATEGORIES) {
        return state.map(item => ({
            ...item,
            checked: false,
        }))
    }

    return state;
};


export default incomeCategoriesReducer;
