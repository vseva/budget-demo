import { combineReducers } from 'redux';
import uniqBy from 'lodash/uniqBy';

import {
    EDIT_ENTRY_REQUEST_SUCCESS,
    NEW_ENTRY_REQUEST_SUCCESS,
    REQUEST_ALL_DATA_FAIL,
    REQUEST_ALL_DATA_START, REQUEST_ALL_DATA_SUCCESS,
    REQUEST_EXPENSES_SUCCESS,
    REQUEST_INCOME_SUCCESS, SET_DATA_CODE,
    UPDATE_EXPENSES_SUGGEST_LISTS, UPDATE_INCOME_SUGGEST_LISTS
} from 'Constant/actions';
import { EXPENSE_ENTRY_TYPE, INCOME_ENTRY_TYPE } from 'Constant';


const dataIsLoadingReducer = (state = false, { type }) => {
    if (type === REQUEST_ALL_DATA_START) return true;
    if (
        type === REQUEST_EXPENSES_SUCCESS ||
        type === REQUEST_INCOME_SUCCESS ||
        type === REQUEST_ALL_DATA_FAIL
    ) {
        return false;
    }

    return state;
};

const dataIsLoadedReducer = (state = false, { type }) => {
    if (type === REQUEST_ALL_DATA_SUCCESS) return true;
    return state;
};

const codeReducer = (state = '', { type, payload }) => {
    if (type === SET_DATA_CODE) return payload;

    return state;
};

const expensesReducer = (state = [], { type, payload }) => {
    if (type === REQUEST_EXPENSES_SUCCESS) return payload;

    if (type === NEW_ENTRY_REQUEST_SUCCESS) {
        const { rowData, rowIdx, entryType } = payload;

        if (entryType === EXPENSE_ENTRY_TYPE) {
            return state.concat({ ...rowData, rowIdx });
        }

        return state;
    }

    if (type === EDIT_ENTRY_REQUEST_SUCCESS) {
        const { rowData, rowIdx, entryType } = payload;

        if (entryType === EXPENSE_ENTRY_TYPE) {
            return state.map(exp => (exp.rowIdx === rowIdx ? { ...rowData, rowIdx } : exp));
        }

        return state;
    }

    return state;
};

const incomeReducer = (state = [], { type, payload }) => {
    if (type === REQUEST_INCOME_SUCCESS) return payload;

    if (type === NEW_ENTRY_REQUEST_SUCCESS) {
        const { rowData, rowIdx, entryType } = payload;

        if (entryType === INCOME_ENTRY_TYPE) {
            return state.concat({ ...rowData, rowIdx });
        }

        return state;
    }

    if (type === EDIT_ENTRY_REQUEST_SUCCESS) {
        const { rowData, rowIdx, entryType } = payload;

        if (entryType === INCOME_ENTRY_TYPE) {
            return state.map(inc => (inc.rowIdx === rowIdx ? { ...rowData, rowIdx } : inc));
        }

        return state;
    }

    return state;
};

const expensesSuggestListsReducer = (state = {}, { type, payload }) => {
    if (type === REQUEST_EXPENSES_SUCCESS) {
        const vendor = uniqBy(payload, 'vendor').map(exp => exp.vendor).sort();
        const description = uniqBy(payload, 'description').map(exp => exp.description).sort();
        const category = uniqBy(payload, 'category').map(exp => exp.category).sort();

        return {
            vendor,
            description,
            category,
        };
    }

    if (type === UPDATE_EXPENSES_SUGGEST_LISTS) {
        const { name, value } = payload;
        const listValues = state[name] || [];

        return {
            ...state,
            [name]: listValues.concat(value).sort(),
        };
    }

    return state;
};

const incomeSuggestListsReducer = (state = {}, { type, payload }) => {
    if (type === REQUEST_INCOME_SUCCESS) {
        const description = uniqBy(payload, 'description').map(exp => exp.description).sort();
        const category = uniqBy(payload, 'category').map(exp => exp.category).sort();

        return {
            description,
            category,
        };
    }

    if (type === UPDATE_INCOME_SUGGEST_LISTS) {
        const { name, value } = payload;
        const listValues = state[name] || [];

        return {
            ...state,
            [name]: listValues.concat(value).sort(),
        };
    }

    return state;
};


export default combineReducers({
    expenses: expensesReducer,
    income: incomeReducer,
    expensesSuggestLists: expensesSuggestListsReducer,
    incomeSuggestLists: incomeSuggestListsReducer,
    dataIsLoading: dataIsLoadingReducer,
    dataIsLoaded: dataIsLoadedReducer,
    code: codeReducer,
});
