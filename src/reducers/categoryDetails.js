import { combineReducers } from 'redux';

import {
    CLOSE_CATEGORY_DETAILS,
    OPEN_CATEGORY_DETAILS,
    SET_CATEGORY_DETAILS_DATE,
} from 'Constant/actions';


const categoryDetailsOpenedReducer = (state = false, { type }) => {
    if (type === OPEN_CATEGORY_DETAILS) return true;
    if (type === CLOSE_CATEGORY_DETAILS) return false;

    return state;
};

const categoryDetailsCategoryReducer = (state = null, { type, payload }) => {
    if (type === SET_CATEGORY_DETAILS_DATE) return payload;

    return state;
};

export default combineReducers({
    opened: categoryDetailsOpenedReducer,
    category: categoryDetailsCategoryReducer
});
