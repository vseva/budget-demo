import { combineReducers } from 'redux';

import {
    CLOSE_VENDOR_DETAILS,
    OPEN_VENDOR_DETAILS, SET_CATEGORY_DETAILS_DATE, SET_VENDOR_DETAILS_VENDOR,
} from 'Constant/actions';


const vendorDetailsOpenedReducer = (state = false, { type }) => {
    if (type === OPEN_VENDOR_DETAILS) return true;
    if (type === CLOSE_VENDOR_DETAILS) return false;

    return state;
};

const vendorDetailsVendorReducer = (state = null, { type, payload }) => {
    if (type === SET_VENDOR_DETAILS_VENDOR) return payload;

    return state;
};


export default combineReducers({
    opened: vendorDetailsOpenedReducer,
    vendor: vendorDetailsVendorReducer,
});
