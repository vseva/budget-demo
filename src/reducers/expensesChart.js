import { combineReducers } from 'redux';

import {
    SET_EXPENSES_CHART_ACTIVE_SECTOR,
    SET_PERIOD_FILTER_END_DATE,
    SET_PERIOD_FILTER_START_DATE,
    TOGGLE_EXPENSES_CATEGORY,
} from 'Constant/actions';


const activeSectorReducer = (state = 0, { type, payload }) => {
    if (type === SET_EXPENSES_CHART_ACTIVE_SECTOR) {
        return payload;
    }

    if (
        type === TOGGLE_EXPENSES_CATEGORY ||
        type === SET_PERIOD_FILTER_END_DATE ||
        type === SET_PERIOD_FILTER_START_DATE
    ) {
        return 0;
    }

    return state;
};


export default combineReducers({
    activeSector: activeSectorReducer,
});
