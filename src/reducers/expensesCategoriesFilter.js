import orderBy from 'lodash/orderBy';
import uniqBy from 'lodash/uniqBy';

import {
    CHECK_ALL_EXPENSES_CATEGORIES,
    REQUEST_EXPENSES_SUCCESS,
    TOGGLE_EXPENSES_CATEGORY,
    UNCHECK_ALL_EXPENSES_CATEGORIES, UPDATE_EXPENSES_SUGGEST_LISTS,
} from 'Constant/actions';
import { getCategoryColor } from 'Util';


const expensesCategoriesReducer = (state = [], { type, payload }) => {
    if (type === REQUEST_EXPENSES_SUCCESS) {
        return orderBy(
            uniqBy(payload, 'category').map(({ category }) => ({
                checked: true,
                color: getCategoryColor(category),
                category,
            })),
            'category'
        );
    }

    if (type === TOGGLE_EXPENSES_CATEGORY) {
        const { category, checked } = payload;

        return state.map(item => {
            if (item.category === category) {
                return { ...item, checked };
            }

            return item;
        });
    }

    if (type === CHECK_ALL_EXPENSES_CATEGORIES) {
        return state.map(item => ({
            ...item,
            checked: true
        }));
    }

    if (type === UNCHECK_ALL_EXPENSES_CATEGORIES) {
        return state.map(item => ({
            ...item,
            checked: false
        }));
    }

    if (type === UPDATE_EXPENSES_SUGGEST_LISTS) {
        const { name, value } = payload;

        if (name === 'category') {
            return orderBy(
                state.concat({
                    category: value,
                    checked: true,
                    color: getCategoryColor(value),
                }),
                'category'
            );
        }

        return state;
    }

    return state;
};


export default expensesCategoriesReducer;
