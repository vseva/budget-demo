import { combineReducers } from 'redux';

import {
    CLOSE_EDIT_ENTRY_MODAL,
    EDIT_ENTRY_FORM_FIELD_UPDATED, EDIT_ENTRY_REQUEST_FAIL,
    EDIT_ENTRY_REQUEST_START,
    EDIT_ENTRY_REQUEST_SUCCESS,
    OPEN_EDIT_ENTRY_MODAL, SET_EDIT_ENTRY_INITIAL_FORM_DATA, SET_EDIT_ENTRY_ROW_IDX
} from 'Constant/actions';


const modalOpenedReducer = (state = false, { type }) => {
    if (type === OPEN_EDIT_ENTRY_MODAL) return true;
    if (type === CLOSE_EDIT_ENTRY_MODAL) return false;
    if (type === EDIT_ENTRY_REQUEST_SUCCESS) return false;

    return state;
};

const editEntryRowIdxReducer = (state = null, { type, payload }) => {
    if (type === SET_EDIT_ENTRY_ROW_IDX) return payload;
    if (
        type === CLOSE_EDIT_ENTRY_MODAL ||
        type === EDIT_ENTRY_REQUEST_SUCCESS
    ) {
        return null;
    }

    return state;
};

const formDataReducer = (state = {}, { type, payload }) => {
    if (type === EDIT_ENTRY_FORM_FIELD_UPDATED) {
        const { name, value } = payload;

        return { ...state, [name]: value };
    }

    if (type === SET_EDIT_ENTRY_INITIAL_FORM_DATA) return payload;
    if (
        type === CLOSE_EDIT_ENTRY_MODAL ||
        type === EDIT_ENTRY_REQUEST_SUCCESS
    ) return {};

    return state;
};

const requestInProgressReducer = (state = false, { type }) => {
    if (type === EDIT_ENTRY_REQUEST_START) return true;
    if (
        type === EDIT_ENTRY_REQUEST_SUCCESS ||
        type === EDIT_ENTRY_REQUEST_FAIL
    ) {
        return false;
    }

    return state;
};


export default combineReducers({
    modalOpened: modalOpenedReducer,
    formData: formDataReducer,
    requestInProgress: requestInProgressReducer,
    editEntryRowIdx: editEntryRowIdxReducer,
});
