import axios from 'axios';
import get from 'lodash/get';

import { getApiUrl } from 'Util';
import {
    NEW_ENTRY_FORM_FIELD_UPDATED,
    NEW_ENTRY_REQUEST_START,
    NEW_ENTRY_REQUEST_SUCCESS,
    CLOSE_NEW_ENTRY_MODAL,
    OPEN_NEW_ENTRY_MODAL,
    NEW_ENTRY_REQUEST_FAIL,
} from 'Constant/actions';
import { periodFilterMaxDateSelector } from 'Selector/periodFilter';
import { setPeriodFilterEndDate, setPeriodFilterMaxDate } from 'Action/periodFilter';


export const newEntryRequestStart = () => ({
    type: NEW_ENTRY_REQUEST_START,
});

export const newEntryRequestSuccess = (data) => ({
    type: NEW_ENTRY_REQUEST_SUCCESS,
    payload: data,
});

export const newEntryRequestFail = (error) => ({
    type: NEW_ENTRY_REQUEST_FAIL,
    payload: error,
});

export const newEntryRequestDone = (data) => (dispatch, getState) => {
    const periodFilterMaxDate = periodFilterMaxDateSelector(getState());
    const { date } = data.rowData;

    if (periodFilterMaxDate < date) {
        dispatch(setPeriodFilterMaxDate(date));
        dispatch(setPeriodFilterEndDate(date));
    }

    dispatch(newEntryRequestSuccess(data));
};

export const newEntryRequest = (data) => (dispatch) => {
    dispatch(newEntryRequestStart());

    return axios
        .post(getApiUrl('add'), data)
            .then((response) => {
                const errors = get(response, 'data.error.errors', []);

                if (errors.length) {
                    alert(errors.map(e => e.message).join('\n'));
                    return dispatch(newEntryRequestFail(errors));
                }

                const { rowIdx, entryType } = response.data;

                return dispatch(newEntryRequestDone({
                    ...data,
                    entryType,
                    rowIdx,
                }));
            });
};

export const newEntry = (formData) => (dispatch) => {
    dispatch(newEntryRequest(formData));
};

export const openNewEntryModal = () => ({
    type: OPEN_NEW_ENTRY_MODAL,
});

export const closeNewEntryModal = () => ({
    type: CLOSE_NEW_ENTRY_MODAL,
});

export const updateNewEntryFormField = (name, value) => ({
    type: NEW_ENTRY_FORM_FIELD_UPDATED,
    payload: {
        name,
        value,
    },
});
