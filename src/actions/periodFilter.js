import moment from 'moment';
import uniq from 'lodash/uniq';

import {
    SET_PERIOD_FILTER_END_DATE,
    SET_PERIOD_FILTER_START_DATE,
    SET_PERIOD_MAX_DATE,
    SET_PERIOD_MIN_DATE
} from 'Constant/actions';
import { expensesSelector } from 'Selector/data/expenses';
import { STORE_DATE_FORMAT } from 'Constant';


export const setPeriodFilterStartDate = (date) => (dispatch, getState) => {
    if (date) {
        return dispatch({
            type: SET_PERIOD_FILTER_START_DATE,
            payload: moment(date).format(STORE_DATE_FORMAT),
        });
    }

    const expensesDates = uniq(expensesSelector(getState()).map(exp => exp.date)).sort();
    const monthStartOfLastDate = moment(expensesDates[expensesDates.length -1])
        .startOf('month')
        .format(STORE_DATE_FORMAT);

    return dispatch({
        type: SET_PERIOD_FILTER_START_DATE,
        payload: monthStartOfLastDate,
    });
};

export const setPeriodFilterEndDate = (date) => (dispatch, getState) => {
    if (!date) {
        const expenses = expensesSelector(getState());

        return dispatch({
            type: SET_PERIOD_FILTER_END_DATE,
            payload: expenses[expenses.length - 1].date,
        });
    }

    return dispatch({
        type: SET_PERIOD_FILTER_END_DATE,
        payload: moment(date).format(STORE_DATE_FORMAT),
    });
};

export const setPeriodFilterMinDate = () => (dispatch, getState) => {
    const expenses = expensesSelector(getState());

    dispatch({
        type: SET_PERIOD_MIN_DATE,
        payload: expenses[0].date,
    });
};

export const setPeriodFilterMaxDate = (date) => ({
    type: SET_PERIOD_MAX_DATE,
    payload: !date ? moment().format(STORE_DATE_FORMAT) : date,
});
