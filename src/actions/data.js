import axios from 'axios';

import {
    REQUEST_ALL_DATA_FAIL,
    REQUEST_ALL_DATA_START, REQUEST_ALL_DATA_SUCCESS,
    REQUEST_EXPENSES_SUCCESS,
    REQUEST_INCOME_SUCCESS, SET_DATA_CODE,
    UPDATE_EXPENSES_SUGGEST_LISTS, UPDATE_INCOME_SUGGEST_LISTS,
} from 'Constant/actions';
import { getApiUrl } from 'Util';
import {
    setPeriodFilterEndDate,
    setPeriodFilterMaxDate,
    setPeriodFilterMinDate,
    setPeriodFilterStartDate
} from 'Action/periodFilter';
import { dataCodeSelector } from 'Selector/data';


export const requestAllDataStart = () => ({
    type: REQUEST_ALL_DATA_START,
});

export const requestAllDataSuccess = () => ({
    type: REQUEST_ALL_DATA_SUCCESS,
});

export const requestAllDataFail = () => ({
    type: REQUEST_ALL_DATA_FAIL,
});

export const requestExpensesSuccess = (data) => ({
    type: REQUEST_EXPENSES_SUCCESS,
    payload: data,
});

export const requestIncomeSuccess = (data) => ({
    type: REQUEST_INCOME_SUCCESS,
    payload: data,
});

export const allDataRequest = (code) => (dispatch) => {
    dispatch(requestAllDataStart());

    return axios
        .get(getApiUrl('pull') + '?code=' + code)
        .then((response) => {
            const responseData = response.data;

            dispatch(requestAllDataSuccess());
            dispatch(requestExpensesSuccess(responseData.expenses));
            dispatch(requestIncomeSuccess(responseData.income));
            dispatch(setPeriodFilterMinDate());
            dispatch(setPeriodFilterMaxDate());
            dispatch(setPeriodFilterStartDate());
            dispatch(setPeriodFilterEndDate());
        })
        .catch(() => {
            dispatch(requestAllDataFail());
        })
};

export const requestAllData = () => (dispatch, getState) => {
    const code = dataCodeSelector(getState());

    dispatch(allDataRequest(code));
};

export const updateExpensesSuggestLists = (name, value) => ({
    type: UPDATE_EXPENSES_SUGGEST_LISTS,
    payload: { name, value },
});

export const updateIncomeSuggestLists = (name, value) => ({
    type: UPDATE_INCOME_SUGGEST_LISTS,
    payload: { name, value },
});

export const setDataCode = (code) => ({
    type: SET_DATA_CODE,
    payload: code,
});
