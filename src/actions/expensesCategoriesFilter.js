import {
    TOGGLE_EXPENSES_CATEGORY,
    CHECK_ALL_EXPENSES_CATEGORIES,
    UNCHECK_ALL_EXPENSES_CATEGORIES,
} from 'Constant/actions';


export const toggleExpensesCategory = ({ checked, category }) => ({
    type: TOGGLE_EXPENSES_CATEGORY,
    payload: { checked, category },
});

export const checkAllExpensesCategories = () => ({
    type: CHECK_ALL_EXPENSES_CATEGORIES,
});

export const uncheckAllExpensesCategories = () => ({
    type: UNCHECK_ALL_EXPENSES_CATEGORIES,
});
