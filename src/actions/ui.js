import {
    CLOSE_EXPENSES_CATEGORIES_FILTER,
    OPEN_EXPENSES_CATEGORIES_FILTER,
    CLOSE_SIDE_MENU,
    OPEN_SIDE_MENU,
    SET_SITE_SECTION,
    SIDE_MENU_STATE_CHANGED, OPEN_INCOME_CATEGORIES_FILTER, CLOSE_INCOME_CATEGORIES_FILTER,
} from 'Constant/actions';
import { expensesCategoriesFilterOpenedSelector, incomeCategoriesFilterOpenedSelector } from 'Selector/ui';


export const setSiteSection = payload => ({
    type: SET_SITE_SECTION,
    payload,
});

export const closeSideMenu = () => ({
    type: CLOSE_SIDE_MENU,
});

export const openSideMenu = () => ({
    type: OPEN_SIDE_MENU,
});

export const sideMenuStateChanged = () => ({
    type: SIDE_MENU_STATE_CHANGED,
});

export const sideMenuStateChange = (state, opened) => {
    if (state.isOpen === opened) return sideMenuStateChanged();

    return state.isOpen ? openSideMenu() : closeSideMenu();
};

export const openExpensesCategoriesFilter = () => ({
    type: OPEN_EXPENSES_CATEGORIES_FILTER,
});

export const closeExpensesCategoriesFilter = () => ({
    type: CLOSE_EXPENSES_CATEGORIES_FILTER,
});

export const toggleExpensesCategoriesFilter = () => (dispatch, getState) => {
    const opened = expensesCategoriesFilterOpenedSelector(getState());

    return opened ?
        dispatch(closeExpensesCategoriesFilter()) :
        dispatch(openExpensesCategoriesFilter());
};

export const openIncomeCategoriesFilter = () => ({
    type: OPEN_INCOME_CATEGORIES_FILTER,
});

export const closeIncomeCategoriesFilter = () => ({
    type: CLOSE_INCOME_CATEGORIES_FILTER,
});

export const toggleIncomeCategoriesFilter = () => (dispatch, getState) => {
    const opened = incomeCategoriesFilterOpenedSelector(getState());

    return opened ?
        dispatch(closeIncomeCategoriesFilter()) :
        dispatch(openIncomeCategoriesFilter());
};
