import {
    OPEN_VENDOR_DETAILS,
    CLOSE_VENDOR_DETAILS, SET_CATEGORY_DETAILS_DATE, SET_VENDOR_DETAILS_VENDOR,
} from 'Constant/actions';

export const openVendorDetails = () => ({
    type: OPEN_VENDOR_DETAILS,
});

export const closeVendorDetails = () => ({
    type: CLOSE_VENDOR_DETAILS,
});

export const setVendorDetailsVendor = (vendor) => ({
    type: SET_VENDOR_DETAILS_VENDOR,
    payload: vendor,
});
