import {
    CLOSE_DAY_DETAILS,
    OPEN_DAY_DETAILS,
    SET_DAY_DETAILS_DATE,
} from 'Constant/actions';


export const setDayDetailsDate = (date) => ({
    type: SET_DAY_DETAILS_DATE,
    payload: date,
});

export const openDayDetails = () => ({
    type: OPEN_DAY_DETAILS,
});

export const closeDayDetails = () => ({
    type: CLOSE_DAY_DETAILS,
});

