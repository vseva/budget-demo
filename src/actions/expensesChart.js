import { SET_EXPENSES_CHART_ACTIVE_SECTOR } from 'Constant/actions';


export const setExpensesChartActiveSector = sector => ({
    type: SET_EXPENSES_CHART_ACTIVE_SECTOR,
    payload: sector,
});
