import {
    CLOSE_CATEGORY_DETAILS,
    OPEN_CATEGORY_DETAILS,
    SET_CATEGORY_DETAILS_DATE,
} from 'Constant/actions';


export const setCategoryDetailsDate = (date) => ({
    type: SET_CATEGORY_DETAILS_DATE,
    payload: date,
});

export const openCategoryDetails = () => ({
    type: OPEN_CATEGORY_DETAILS,
});

export const closeCategoryDetails = () => ({
    type: CLOSE_CATEGORY_DETAILS,
});

