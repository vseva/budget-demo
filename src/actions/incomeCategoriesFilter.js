import {
    TOGGLE_INCOME_CATEGORY,
    CHECK_ALL_INCOME_CATEGORIES,
    UNCHECK_ALL_INCOME_CATEGORIES
} from 'Constant/actions';


export const toggleIncomeCategory = ({ checked, category }) => ({
    type: TOGGLE_INCOME_CATEGORY,
    payload: { checked, category },
});

export const checkAllIncomeCategories = () => ({
    type: CHECK_ALL_INCOME_CATEGORIES,
});

export const uncheckAllIncomeCategories = () => ({
    type: UNCHECK_ALL_INCOME_CATEGORIES,
});
