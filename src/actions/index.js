import { SET_ENTRY_TYPE } from 'Constant/actions';


export const setEntryType = type => ({
    type: SET_ENTRY_TYPE,
    payload: type,
});
