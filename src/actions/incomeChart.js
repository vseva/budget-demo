import { SET_INCOME_CHART_ACTIVE_SECTOR } from 'Constant/actions';


export const setIncomeChartActiveSector = sector => ({
    type: SET_INCOME_CHART_ACTIVE_SECTOR,
    payload: sector,
});
