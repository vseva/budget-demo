import axios from 'axios';
import get from 'lodash/get';

import { getApiUrl } from 'Util';
import {
    OPEN_EDIT_ENTRY_MODAL,
    CLOSE_EDIT_ENTRY_MODAL,
    EDIT_ENTRY_REQUEST_START,
    EDIT_ENTRY_REQUEST_SUCCESS,
    EDIT_ENTRY_FORM_FIELD_UPDATED, SET_EDIT_ENTRY_ROW_IDX, SET_EDIT_ENTRY_INITIAL_FORM_DATA, EDIT_ENTRY_REQUEST_FAIL,
} from 'Constant/actions';
import { periodFilterMaxDateSelector } from 'Selector/periodFilter';
import { setPeriodFilterMaxDate } from 'Action/periodFilter';
import { editEntryRowIdxSelector } from 'Selector/editEntry';
import { setEntryType } from 'Action/index';


export const editEntryRequestStart = () => ({
    type: EDIT_ENTRY_REQUEST_START,
});

export const editEntryRequestSuccess = (data) => ({
    type: EDIT_ENTRY_REQUEST_SUCCESS,
    payload: data,
});

export const editEntryRequestFail = (error) => ({
    type: EDIT_ENTRY_REQUEST_FAIL,
    payload: error,
});

export const editEntryRequestDone = (data) => (dispatch, getState) => {
    const periodFilterMaxDate = periodFilterMaxDateSelector(getState());
    const { date } = data.rowData;

    if (periodFilterMaxDate < date) {
        dispatch(setPeriodFilterMaxDate(date));
    }

    dispatch(editEntryRequestSuccess(data));
};

export const editEntryRequest = (data) => (dispatch, getState) => {
    dispatch(editEntryRequestStart());

    const rowIdx = editEntryRowIdxSelector(getState());
    const postData = { ...data, rowIdx };

    return axios
        .post(getApiUrl('edit'), postData)
            .then((response) => {
                const errors = get(response, 'data.error.errors', []);

                if (errors.length) {
                    alert(errors.map(e => e.message).join('\n'));
                    return dispatch(editEntryRequestFail(errors));
                }

                return dispatch(editEntryRequestDone(postData))
            });
};

export const editEntry = (formData) => (dispatch) => {
    dispatch(editEntryRequest(formData));
};

export const openEditEntryModal = () => ({
    type: OPEN_EDIT_ENTRY_MODAL,
});

export const setEditedEntryRowIdx = rowIdx => ({
    type: SET_EDIT_ENTRY_ROW_IDX,
    payload: rowIdx,
});

export const setEditEntryInitialFormData = formData => ({
    type: SET_EDIT_ENTRY_INITIAL_FORM_DATA,
    payload: formData,
});

export const openEditEntry = (cellInfoOriginal, entryType) => (dispatch) => {
    const { rowIdx } = cellInfoOriginal;
    const initialFormData = Object.keys(cellInfoOriginal)
        .filter(k => k !== 'rowIdx')
        .reduce((acc, cur) => {
            acc[cur] = cellInfoOriginal[cur];
            return acc;
        }, {});

    dispatch(setEntryType(entryType));
    dispatch(setEditedEntryRowIdx(rowIdx));
    dispatch(setEditEntryInitialFormData(initialFormData));
    dispatch(openEditEntryModal(cellInfoOriginal));
};

export const closeEditEntryModal = () => ({
    type: CLOSE_EDIT_ENTRY_MODAL,
});

export const updateEditEntryFormField = (name, value) => ({
    type: EDIT_ENTRY_FORM_FIELD_UPDATED,
    payload: {
        name,
        value,
    },
});
