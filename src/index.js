import React from 'react';
import ReactModal from 'react-modal';
import { render } from 'react-dom';
import { Provider } from 'react-redux';

import App from 'Component/App';
import configureStore from 'Store';

import 'Image/favicon.png';


const store = configureStore();

ReactModal.setAppElement('#app');
ReactModal.defaultStyles.content.maxWidth = '400px';
ReactModal.defaultStyles.content.marginLeft = 'auto';
ReactModal.defaultStyles.content.marginRight = 'auto';

render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById('app')
);
