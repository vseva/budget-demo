export const COLORS = [
    { value: '#0088FE', label: 'Электроника' },
    { value: '#00C49F', label: 'Кредит' },
    { value: '#FFBB28', label: 'Еда' },
    { value: '#ff1522', label: 'Одежда/обувь' },
    { value: '#bb57fe', label: 'Товары для дома' },
    { value: '#3fd746', label: 'Подарки' },
    { value: '#a71d32', label: 'Транспорт' },
    { value: '#86d9ff', label: 'Здоровье Жени' },
    { value: '#6f6391', label: 'Еда в офисе' },
    { value: '#4bac41', label: 'Красота' },
    { value: '#c9da1e', label: 'Здоровье' },
    { value: '#5938ff', label: 'Алкоголь' },
    { value: '#c982dd', label: 'Гигиена Жени' },
    { value: '#46f0d3', label: 'Курение' },
    { value: '#ff5d36', label: 'Еда Жени' },
    { value: '#ff8042', label: 'Развлечения' },
    { value: '#38c43f', label: 'Связь' },
    { value: '#380efe', label: 'Коммуналка' },
    { value: '#818d37', label: 'Цифровые услуги' },
    { value: '#392a09', label: 'Сыр' },
    { value: '#3bff40', label: 'Чтение' },
    { value: '#ff56fb', label: 'Копилка' },
    { value: '#59bbff', label: 'Другое' },

    { value: '#a79963', label: 'Зарплата' },
    { value: '#27ac6f', label: 'Фриланс' },
    { value: '#f3e35b', label: 'Авито' },
    { value: '#f3345e', label: 'Пособие' },
    { value: '#5271f3', label: 'Копилка' },
    { value: '#f372eb', label: 'Проценты' },
];

export const EXPENSES_COLOR = '#ff2d00';
export const INCOME_COLOR = '#008b00';

export const CHART_ANIMATION_DURATION = 800;
export const DISPLAY_MOMENT_DATE_FORMAT = 'DD.MM.YYYY';
export const HUMAN_MOMENT_DATE_FORMAT = 'D MMM YYYY';
export const DATE_FORMAT = 'dd.MM.yyyy';
export const STORE_DATE_FORMAT = 'YYYY-MM-DD';

export const EXPENSES_SITE_SECTION = 'EXPENSES_SITE_SECTION';
export const INCOME_SITE_SECTION = 'INCOME_SITE_SECTION';

export const SITE_SECTIONS = [
    {
        id: EXPENSES_SITE_SECTION,
        title: 'Expenses'
    },
    {
        id: INCOME_SITE_SECTION,
        title: 'Income'
    },
];

export const EXPENSES_CATEGORIES_FILTER = 'EXPENSES_CATEGORIES_FILTER';
export const INCOME_CATEGORIES_FILTER = 'INCOME_CATEGORIES_FILTER';

export const EXPENSES_PIE_CHART = 'EXPENSES_PIE_CHART';
export const INCOME_PIE_CHART = 'INCOME_PIE_CHART';

export const EXPENSES_BAR_CHART = 'EXPENSES_BAR_CHART';
export const INCOME_BAR_CHART = 'INCOME_BAR_CHART';

export const PIE_CHART_TYPE = 'PIE_CHART_TYPE';
export const BAR_CHART_TYPE = 'BAR_CHART_TYPE';
export const BUBBLE_CHART_TYPE = 'BUBBLE_CHART_TYPE';

export const EXPENSES_FULL_TABLE = 'EXPENSES_FULL_TABLE';
export const INCOME_FULL_TABLE = 'INCOME_FULL_TABLE';

export const EXPENSE_ENTRY_TYPE = 'EXPENSE_ENTRY_TYPE';
export const INCOME_ENTRY_TYPE = 'INCOME_ENTRY_TYPE';
