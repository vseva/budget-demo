import { createSelector } from 'reselect';


export const expensesCategoriesFilterSelector = state => state.expensesCategoriesFilter;

export const checkedExpensesCategoriesSelector = createSelector(
    expensesCategoriesFilterSelector,
    categories => categories
        .filter(category => category.checked)
        .map(cat => cat.category),
);

export const checkedExpensesCategoriesWithColorSelector = createSelector(
    expensesCategoriesFilterSelector,
    categories => categories
        .filter(category => category.checked)
        .map(({ color, category }) => ({ color, category }))
);
