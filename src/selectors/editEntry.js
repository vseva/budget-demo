import { createSelector } from 'reselect';


export const editEntrySelector = state => state.editEntry;

export const editEntryModalOpenedSelector = createSelector(
    editEntrySelector,
    editEntry => editEntry.modalOpened
);

export const editEntryFormDataSelector = createSelector(
    editEntrySelector,
    editEntry => editEntry.formData
);

export const editEntryRequestInProgressSelector = createSelector(
    editEntrySelector,
    editEntry => editEntry.requestInProgress
);

export const editEntryRowIdxSelector = createSelector(
    editEntrySelector,
    editEntry => editEntry.editEntryRowIdx
);
