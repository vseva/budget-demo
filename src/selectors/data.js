import { createSelector } from 'reselect';


export const dataSelector = state => state.data;

export const dataIsLoadingSelector = createSelector(
    dataSelector,
    data => data.dataIsLoading
);

export const dataIsLoadedSelector = createSelector(
    dataSelector,
    data => data.dataIsLoaded
);

export const dataCodeSelector = createSelector(
    dataSelector,
    data => data.code
);
