import { createSelector } from 'reselect';


export const vendorDetailsSelector = state => state.vendorDetails;

export const vendorDetailsOpenedSelector = createSelector(
    vendorDetailsSelector,
    vendorDetails => vendorDetails.opened
);

export const vendorDetailsVendorSelector = createSelector(
    vendorDetailsSelector,
    vendorDetails => vendorDetails.vendor
);
