import { createSelector } from 'reselect';


export const newEntrySelector = state => state.newEntry;

export const newEntryModalOpenedSelector = createSelector(
    newEntrySelector,
    newEntry => newEntry.modalOpened
);

export const newEntryFormDataSelector = createSelector(
    newEntrySelector,
    newEntry => newEntry.formData
);

export const newEntryRequestInProgressSelector = createSelector(
    newEntrySelector,
    newEntry => newEntry.requestInProgress
);

export const newEntryTypeSelector = createSelector(
    newEntrySelector,
    newEntry => newEntry.entryType
);
