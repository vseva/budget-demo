import { createSelector } from 'reselect';


export const dayDetailsSelector = state => state.dayDetails;

export const dayDetailsOpenedSelector = createSelector(
    dayDetailsSelector,
    dayDetails => dayDetails.opened
);

export const dayDetailsDateSelector = createSelector(
    dayDetailsSelector,
    dayDetails => dayDetails.date
);
