import { createSelector } from 'reselect';


export const expensesChartSelector = state => state.expensesChart;

export const expensesChartActiveSectorSelector = createSelector(
    expensesChartSelector,
    chart => chart.activeSector
);
