import { createSelector } from 'reselect';
import find from 'lodash/find';
import includes from 'lodash/includes';
import groupBy from 'lodash/groupBy';
import orderBy from 'lodash/orderBy';

import { periodFilterEndDateSelector, periodFilterStartDateSelector } from 'Selector/periodFilter';
import { checkedExpensesCategoriesSelector } from 'Selector/expensesCategoriesFilter';
import { dataSelector } from 'Selector/data';
import { groupExpensesData } from 'Util';
import { editEntryRowIdxSelector } from 'Selector/editEntry';


export const expensesSelector = createSelector(
    dataSelector,
    data => data.expenses
);

export const filteredExpensesSelector = createSelector([
    expensesSelector,
    checkedExpensesCategoriesSelector,
    periodFilterStartDateSelector,
    periodFilterEndDateSelector,
], (expenses, checkedCategories, start, end) => expenses.filter(exp => (
    includes(checkedCategories, exp.category) &&
    exp.date >= start &&
    exp.date <= end
)));

export const filteredExpensesTotalSelector = createSelector(
    filteredExpensesSelector,
    expenses => expenses.reduce((acc, cur) => acc += cur.amount, 0)
);

export const expensesByCategoriesFilteredByDatesAndCategoriesSelector = createSelector(
    filteredExpensesSelector,
    expenses => groupExpensesData(expenses)
);

export const filteredExpensesByDaySelector = (state, dayDate, groupByCategory) => createSelector(
    filteredExpensesSelector,
    expenses => {
        const filteredExpenses = expenses.filter(exp => dayDate === exp.date);

        if (!groupByCategory) return filteredExpenses;

        const groupedExpenses = groupBy(filteredExpenses, 'category');
        const categories = Object.keys(groupedExpenses);
        const categoriesData = categories.reduce((acc, category) => acc.concat({
            total: groupedExpenses[category].reduce((acc, cur) => acc + cur.amount, 0),
            items: orderBy(groupedExpenses[category], 'amount', 'desc'),
            category,
        }), []);

        return orderBy(categoriesData, 'total', 'desc');
    }
)(state);

export const filteredExpensesByCategorySelector = (state, category) => createSelector(
    filteredExpensesSelector,
    expenses => expenses.filter(exp => category === exp.category)
)(state);

export const filteredExpensesGroupedByVendorSelector = createSelector(
    filteredExpensesSelector,
    expenses => groupBy(expenses, 'vendor'),
);

export const filteredExpensesByVendorSelector = (state, vendor) => createSelector(
    filteredExpensesSelector,
    expenses => expenses.filter(exp => vendor === exp.vendor)
)(state);

export const filteredExpensesForTimeBarChart = createSelector(
    filteredExpensesSelector,
    expenses => {
        const groupedByDay = groupBy(expenses, 'date');
        const days = Object.keys(groupedByDay).sort();
        const groupedByCategories = [];

        days.forEach((day) => {
            const dayExpenses = groupedByDay[day];
            const categorizedDay = {
                date: day,
            };

            dayExpenses.forEach(({ category, amount }) => {
                const value = categorizedDay[category] !== undefined ?
                    categorizedDay[category] :
                    0;

                categorizedDay[category] = value + amount;
            });

            groupedByCategories.push(categorizedDay);

        });

        return groupedByCategories;
    }
);

export const filteredByDatesExpensesSelector = createSelector([
    expensesSelector,
    periodFilterStartDateSelector,
    periodFilterEndDateSelector,
], (expenses, start, end) => expenses.filter(exp => (
    exp.date >= start &&
    exp.date <= end
)));

export const filteredByDatesTotalExpensesSelector = createSelector(
    filteredByDatesExpensesSelector,
    expenses => expenses.reduce((acc, cur) => acc += cur.amount, 0)
);

export const expensesSuggestListsSelector = createSelector(
    dataSelector,
    data => data.expensesSuggestLists
);

export const expenseByRowIdxSelector = createSelector([
    editEntryRowIdxSelector,
    expensesSelector,
], (rowIdx, expenses) => find(expenses, { rowIdx })
);
