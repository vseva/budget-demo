import { createSelector } from 'reselect';
import includes from 'lodash/includes';

import { dataSelector } from 'Selector/data';
import { checkedIncomeCategoriesSelector } from 'Selector/incomeCategoriesFilter';
import { periodFilterEndDateSelector, periodFilterStartDateSelector } from 'Selector/periodFilter';
import { groupIncomeData } from 'Util';
import groupBy from 'lodash/groupBy';
import { filteredExpensesSelector } from 'Selector/data/expenses';


export const incomeSelector = createSelector(
    dataSelector,
    data => data.income
);

export const filteredIncomeSelector = createSelector([
    incomeSelector,
    checkedIncomeCategoriesSelector,
    periodFilterStartDateSelector,
    periodFilterEndDateSelector,
], (income, checkedCategories, start, end) => income.filter(inc => (
    includes(checkedCategories, inc.category) &&
    inc.date >= start &&
    inc.date <= end
)));

export const filteredIncomeTotalSelector = createSelector(
    filteredIncomeSelector,
    income => income.reduce((acc, cur) => acc += cur.amount, 0)
);

export const incomeByCategoriesFilteredByDatesAndCategoriesSelector = createSelector(
    filteredIncomeSelector,
    income => groupIncomeData(income)
);

export const filteredIncomeForTimeBarChart = createSelector(
    filteredIncomeSelector,
    income => {
        const groupedByDay = groupBy(income, 'date');
        const days = Object.keys(groupedByDay).sort();
        const groupedByCategories = [];

        days.forEach((day) => {
            const dayIncome = groupedByDay[day];
            const categorizedDay = {
                date: day,
            };

            dayIncome.forEach(({ category, amount }) => {
                const value = categorizedDay[category] !== undefined ?
                    categorizedDay[category] :
                    0;

                categorizedDay[category] = value + amount;
            });

            groupedByCategories.push(categorizedDay);

        });

        return groupedByCategories;
    }
);

export const filteredByDatesIncomeSelector = createSelector([
    incomeSelector,
    periodFilterStartDateSelector,
    periodFilterEndDateSelector,
], (income, start, end) => income.filter(income => (
    income.date >= start &&
    income.date <= end
)));

export const filteredByDatesTotalIncomeSelector = createSelector(
    filteredByDatesIncomeSelector,
    income => income.reduce((acc, cur) => acc += cur.amount, 0)
);

export const incomeSuggestListsSelector = createSelector(
    dataSelector,
    data => data.incomeSuggestLists
);