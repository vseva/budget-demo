import { createSelector } from 'reselect';


export const uiSelector = state => state.ui;

export const currentSiteSectionSelector = createSelector(
    uiSelector,
    ui => ui.currentSection,
);

export const isSideMenuOpenedSelector = createSelector(
    uiSelector,
    ui => ui.sideMenu
);

export const expensesCategoriesFilterOpenedSelector = createSelector(
    uiSelector,
    ui => ui.expensesCategoriesFilterOpened
);

export const incomeCategoriesFilterOpenedSelector = createSelector(
    uiSelector,
    ui => ui.incomeCategoriesFilterOpened
);
