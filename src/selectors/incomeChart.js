import { createSelector } from 'reselect';


export const incomeChartSelector = state => state.incomeChart;

export const incomeChartActiveSectorSelector = createSelector(
    incomeChartSelector,
    chart => chart.activeSector
);
