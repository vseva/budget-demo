import { createSelector } from 'reselect';
import { expensesCategoriesFilterSelector } from 'Selector/expensesCategoriesFilter';


export const incomeCategoriesFilterSelector = state => state.incomeCategoriesFilter;

export const checkedIncomeCategoriesSelector = createSelector(
    incomeCategoriesFilterSelector,
    categories => categories
        .filter(category => category.checked)
        .map(cat => cat.category),
);

export const checkedIncomeCategoriesWithColorSelector = createSelector(
    incomeCategoriesFilterSelector,
    categories => categories
        .filter(category => category.checked)
        .map(({ color, category }) => ({ color, category }))
);