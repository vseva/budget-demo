import { createSelector } from 'reselect';


export const categoryDetailsSelector = state => state.categoryDetails;

export const categoryDetailsOpenedSelector = createSelector(
    categoryDetailsSelector,
    categoryDetails => categoryDetails.opened
);

export const categoryDetailsCategorySelector = createSelector(
    categoryDetailsSelector,
    categoryDetails => categoryDetails.category
);
