import { createSelector } from 'reselect';


export const periodFilterSelector = state => state.periodFilter;

export const periodFilterStartDateSelector = createSelector(
    periodFilterSelector,
    periodFilter => periodFilter.start
);

export const periodFilterEndDateSelector = createSelector(
    periodFilterSelector,
    periodFilter => periodFilter.end
);

export const periodFilterMinDateSelector = createSelector(
    periodFilterSelector,
    periodFilter => periodFilter.minDate
);

export const periodFilterMaxDateSelector = createSelector(
    periodFilterSelector,
    periodFilter => periodFilter.maxDate
);
