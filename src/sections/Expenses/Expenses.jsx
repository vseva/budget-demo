import React, { Component, Fragment } from 'react';

import TotalAmount from 'Component/TotalAmount';
import CategoriesFilter from 'Component/CategoriesFilter';
import TimeBarChart from 'Component/TimeBarChart';
import CategoriesPieChart from 'Component/CategoriesPieChart';
import FullTable from 'Component/FullTable';
import VendorBubbleChart from 'Component/VendorBubbleChart';
import {
    BAR_CHART_TYPE, BUBBLE_CHART_TYPE,
    EXPENSES_BAR_CHART,
    EXPENSES_CATEGORIES_FILTER,
    EXPENSES_FULL_TABLE,
    EXPENSES_PIE_CHART,
    PIE_CHART_TYPE
} from 'Constant';
import {
    ChartSelectsWrap,
    ChartSelect
} from 'Section/Common.styled';


const charts = [
    {
        label: 'By Category',
        value: PIE_CHART_TYPE,
        chart: <CategoriesPieChart type={EXPENSES_PIE_CHART} />
    },
    {
        label: 'By Day',
        value: BAR_CHART_TYPE,
        chart: <TimeBarChart type={EXPENSES_BAR_CHART} />
    },
    {
        label: 'By Vendor',
        value: BUBBLE_CHART_TYPE,
        chart: <VendorBubbleChart />
    }
];

class Expenses extends Component {
    constructor(props) {
        super(props);

        this.state = {
            openedChart: BAR_CHART_TYPE,
        };

        this.showChart = this.showChart.bind(this);
    }

    showChart(chartType) {
        this.setState({
            openedChart: chartType,
        });
    }

    render() {
        const {
            openedChart
        } = this.state;

        const chart = charts.find(chart => chart.value === openedChart).chart;

        return (
            <Fragment>
                <CategoriesFilter type={EXPENSES_CATEGORIES_FILTER} />
                <TotalAmount />
                <ChartSelectsWrap>
                    {
                        charts.map(({ label, value }) => (
                            <ChartSelect
                                onClick={() => { this.showChart(value) }}
                                key={value}
                                value={value}
                                openedChart={openedChart}
                            >
                                { label }
                            </ChartSelect>
                        ))
                    }
                </ChartSelectsWrap>
                <div>{ chart }</div>
                <FullTable type={EXPENSES_FULL_TABLE} />
            </Fragment>
        );
    }
}


export default Expenses;
