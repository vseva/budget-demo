import React, { Component, Fragment } from 'react';

import TotalAmount from 'Component/TotalAmount';
import CategoriesFilter from 'Component/CategoriesFilter';
import CategoriesPieChart from 'Component/CategoriesPieChart';
import TimeBarChart from 'Component/TimeBarChart';
import FullTable from 'Component/FullTable';
import {
    INCOME_PIE_CHART,
    INCOME_CATEGORIES_FILTER,
    BAR_CHART_TYPE,
    PIE_CHART_TYPE,
    INCOME_BAR_CHART, INCOME_FULL_TABLE,
} from 'Constant';
import {
    ChartSelectsWrap,
    ChartSelect
} from 'Section/Common.styled';


const charts = [
    {
        label: 'By Category',
        value: PIE_CHART_TYPE,
        chart: <CategoriesPieChart type={INCOME_PIE_CHART} />
    },
    {
        label: 'By Day',
        value: BAR_CHART_TYPE,
        chart: <TimeBarChart type={INCOME_BAR_CHART} />
    }
];


class Income extends Component {
    constructor(props) {
        super(props);

        this.state = {
            openedChart: BAR_CHART_TYPE,
        };

        this.showChart = this.showChart.bind(this);
    }

    showChart(chartType) {
        this.setState({
            openedChart: chartType,
        });
    }

    render() {
        const {
            openedChart
        } = this.state;

        const chart = charts.find(chart => chart.value === openedChart).chart;

        return (
            <Fragment>
                <CategoriesFilter type={INCOME_CATEGORIES_FILTER} />
                <TotalAmount />
                <ChartSelectsWrap>
                    {
                        charts.map(({ label, value }) => (
                            <ChartSelect
                                onClick={() => { this.showChart(value) }}
                                key={value}
                                value={value}
                                openedChart={openedChart}
                            >
                                { label }
                            </ChartSelect>
                        ))
                    }
                </ChartSelectsWrap>
                <div>{ chart }</div>
                <FullTable type={INCOME_FULL_TABLE} />
            </Fragment>
        );
    }
}


export default Income;
