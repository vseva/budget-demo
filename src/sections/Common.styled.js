import styled from 'styled-components';


export const ChartSelectsWrap = styled.div`
  margin: 20px 0;
  text-align: center;
  font-size: 12px;
`;

const chartSelectProps = {
    borderBottomColor: ({ value, openedChart }) => (value === openedChart ? 'transparent' : 'grey'),
    cursor: ({ value, openedChart }) => (value === openedChart ? 'default' : 'pointer'),
    fontWeight: ({ value, openedChart }) => (value === openedChart ? 'bold' : 'normal'),
};

export const ChartSelect = styled.span`
  cursor: ${chartSelectProps.cursor};
  margin: 0 10px;
  display: inline-block;
  text-transform: uppercase;
  border-bottom-style: dotted;
  border-bottom-color: ${chartSelectProps.borderBottomColor};
  border-bottom-width: 1px;
  font-weight: ${chartSelectProps.fontWeight};
`;