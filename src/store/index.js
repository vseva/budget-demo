import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';

import rootReducer from 'Reducer';


const middlewareWrapper = DEV_MODE ?
    composeWithDevTools(
        applyMiddleware(thunk)
    ) :
    applyMiddleware(thunk);

const storeCreator = () => createStore(
    rootReducer,
    middlewareWrapper,
);


export default storeCreator;
