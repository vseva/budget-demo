import React, { Component } from 'react';
import includes from 'lodash/includes';
import {
    ResponsiveContainer,
    PieChart,
    Pie,
    Sector,
    Cell,
} from 'recharts';

import { CHART_ANIMATION_DURATION, EXPENSES_PIE_CHART } from 'Constant';
import { priceFormat } from 'Util';
import { openCategoryDetails, setCategoryDetailsDate } from 'Action/categoryDetails';


class CategoriesPieChart extends Component {
    constructor(props) {
        super(props);

        this.renderActiveSector = this.renderActiveSector.bind(this);
    }

    renderActiveSector({
        cx,
        cy,
        innerRadius,
        outerRadius,
        startAngle,
        endAngle,
        payload,
        percent,
        value,
        fill,
    }) {
        const { openCategoryDetails, setCategoryDetailsDate } = this.props;
        const onClick = () => { setCategoryDetailsDate(payload.category); openCategoryDetails(); };

        return (
            <g onClick={onClick}>
                <text
                    x={cx}
                    y={cy}
                    dy={-20}
                    textAnchor="middle"
                    fill={fill}
                    fontSize={24}
                >
                    { `${(percent * 100).toFixed(2)} %` }
                </text>
                <text
                    x={cx}
                    y={cy}
                    dy={8}
                    textAnchor="middle"
                    fill={fill}
                    fontSize={16}
                    fontWeight="bold"
                >
                    { payload.category }
                </text>
                <text
                    x={cx}
                    y={cy}
                    dy={40}
                    textAnchor="middle"
                    fill={fill}
                    fontSize={18}
                >
                    { priceFormat(value, { textString: true }) }
                </text>
                <Sector
                    cx={cx}
                    cy={cy}
                    innerRadius={innerRadius}
                    outerRadius={outerRadius}
                    startAngle={startAngle}
                    endAngle={endAngle}
                    fill={fill}
                />
                <Sector
                    cx={cx}
                    cy={cy}
                    startAngle={startAngle}
                    endAngle={endAngle}
                    innerRadius={outerRadius + 4}
                    outerRadius={outerRadius + 16}
                    fill={fill}
                />
            </g>
        );
    }

    render() {
        const {
            data,
            type,
            activeSector,
            setExpensesChartActiveSector,
            setIncomeChartActiveSector,
        } = this.props;

        if (!data.length) return null;

        const activeSectorSetter = type === EXPENSES_PIE_CHART ?
            setExpensesChartActiveSector :
            setIncomeChartActiveSector;
        const setActiveSector = (data, sector) => activeSectorSetter(sector);

        return (
            <ResponsiveContainer height={350}>
                <PieChart>
                    <Pie
                        dataKey="categoryAmount"
                        activeIndex={activeSector}
                        activeShape={this.renderActiveSector}
                        data={data}
                        innerRadius={80}
                        outerRadius={120}
                        onClick={setActiveSector}
                        animationDuration={CHART_ANIMATION_DURATION}
                    >
                        {
                            data.map((entry, index) => (
                                <Cell key={`${entry.category}${index}`} fill={entry.color} />
                            ))
                        }
                    </Pie>
                </PieChart>
            </ResponsiveContainer>
        );
    }
}


export default CategoriesPieChart;
