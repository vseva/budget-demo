import superHoc from 'Component/superHoc';
import {
    incomeByCategoriesFilteredByDatesAndCategoriesSelector,
} from 'Selector/data/income';
import CategoriesPieChart from './CategoriesPieChart';
import { setExpensesChartActiveSector } from 'Action/expensesChart';
import { expensesChartActiveSectorSelector } from 'Selector/expensesChart';
import { EXPENSES_PIE_CHART } from 'Constant';
import { incomeChartActiveSectorSelector } from 'Selector/incomeChart';
import { setIncomeChartActiveSector } from 'Action/incomeChart';
import {
    expensesByCategoriesFilteredByDatesAndCategoriesSelector,
} from 'Selector/data/expenses';
import { openCategoryDetails, setCategoryDetailsDate } from 'Action/categoryDetails';


const mapStateToProps = (state, { type }) => {
    const isExpensesChart = type === EXPENSES_PIE_CHART;
    const dataSelector = isExpensesChart ?
        expensesByCategoriesFilteredByDatesAndCategoriesSelector :
        incomeByCategoriesFilteredByDatesAndCategoriesSelector;
    const activeSectorSelector = isExpensesChart ?
        expensesChartActiveSectorSelector :
        incomeChartActiveSectorSelector;

    return {
        data: dataSelector(state),
        activeSector: activeSectorSelector(state),
    };
};
const mapDispatchToProps = {
    setExpensesChartActiveSector,
    setIncomeChartActiveSector,
    openCategoryDetails,
    setCategoryDetailsDate,
};
const connectExpensesChart = superHoc(CategoriesPieChart, { mapStateToProps, mapDispatchToProps });


export default connectExpensesChart;
