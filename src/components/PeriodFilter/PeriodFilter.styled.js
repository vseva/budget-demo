import styled from 'styled-components';


export const Container = styled.div`
    margin: 0 auto;
    width: 200px;
`;

export const DatePickerWrap = styled.div`
  display: inline-block;
  margin: 0 5px;
  
  input {
    padding: 7px 6px;
    text-align: center;
    font-size: 12px;
    cursor: pointer;
    border-radius: 3px;
    border: 1px solid #bdbdbd;
    width: 90px;
    outline: none;
    box-shadow: none;
  }
`;
