import React from 'react';
import Datepicker from 'react-datepicker';
import moment from 'moment';
import 'react-datepicker/dist/react-datepicker.css';

import { DATE_FORMAT } from 'Constant';
import {
    Container,
    DatePickerWrap,
} from './PeriodFilter.styled';


const PeriodFilter = ({
    startDate,
    endDate,
    minDate,
    maxDate,
    setPeriodFilterStartDate,
    setPeriodFilterEndDate
}) => {
    const dynamicMaxDate = maxDate <= endDate ? maxDate : endDate;
    const dynamicMinDate = minDate >= startDate ? minDate : startDate;

    return (
        <Container>
            <DatePickerWrap>
                <Datepicker
                    selected={new Date(startDate)}
                    onChange={setPeriodFilterStartDate}
                    dateFormat={DATE_FORMAT}
                    minDate={new Date(minDate)}
                    maxDate={new Date(dynamicMaxDate)}
                />
            </DatePickerWrap>
            <DatePickerWrap>
                <Datepicker
                    selected={new Date(endDate)}
                    onChange={setPeriodFilterEndDate}
                    dateFormat={DATE_FORMAT}
                    maxDate={new Date(maxDate)}
                    minDate={new Date(dynamicMinDate)}
                />
            </DatePickerWrap>
        </Container>
    );
};


export default PeriodFilter;
