import superHoc from 'Component/superHoc';
import PeriodFilter from './PeriodFilter';
import {
    periodFilterEndDateSelector, periodFilterMaxDateSelector, periodFilterMinDateSelector,
    periodFilterStartDateSelector,
} from 'Selector/periodFilter';
import {
    setPeriodFilterEndDate,
    setPeriodFilterStartDate,
} from 'Action/periodFilter';


const mapStateToProps = state => ({
    startDate: periodFilterStartDateSelector(state),
    endDate: periodFilterEndDateSelector(state),
    minDate: periodFilterMinDateSelector(state),
    maxDate: periodFilterMaxDateSelector(state),
});
const mapDispatchToProps = {
    setPeriodFilterStartDate,
    setPeriodFilterEndDate,
};
const connectPeriodFilter = superHoc(PeriodFilter, { mapStateToProps, mapDispatchToProps });


export default connectPeriodFilter;
