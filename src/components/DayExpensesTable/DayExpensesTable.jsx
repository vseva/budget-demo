import React, { Component, Fragment } from 'react';
import moment from 'moment';
import Swipe from 'react-easy-swipe';

import { priceFormat } from 'Util';
import ArrowLeft from 'Image/ArrowLeft';
import ArrowRight from 'Image/ArrowRight';
import {
    Container,
    Table,
    Tr,
    Td,
    RightCol,
    CloseButton,
    DayCategoryDetailsTable,
    DayCategoryDetailsTableColumn,
    DayCategoryDetailsTableRightColumn,
    HeaderWrap,
    PrevNextDayButton,
    CategoryColor,
} from './DayexpensesTable.styled';
import { HUMAN_MOMENT_DATE_FORMAT, STORE_DATE_FORMAT } from 'Constant';


class DayExpensesTable extends Component {
    constructor(props) {
        super(props);

        this.state = {
            categories: {},
        };

        this.toggleDetails = this.toggleDetails.bind(this);
        this.onSwipeMove = this.onSwipeMove.bind(this);
        this.onSwipeEnd = this.onSwipeEnd.bind(this);
    }

    toggleDetails(category) {
        const categoryState = this.state.categories[category];
        const isOpened = categoryState && categoryState.opened;

        this.setState({
            categories: {
                ...this.state.categories,
                [category]: {
                    ...categoryState,
                    opened: !isOpened,
                },
            },
        });
    }

    onSwipeMove(position) {
        this.horizontalDiff = position.x;
    }

    onSwipeEnd() {
        const {
            setDayDetailsDate,
            dayDetailsDate,
        } = this.props;

        if (this.horizontalDiff > 0) {
            const dayBack = moment(dayDetailsDate).subtract(1, 'days').format(STORE_DATE_FORMAT);

            setDayDetailsDate(dayBack);
        } else if (this.horizontalDiff < 0) {
            const dayForward = moment(dayDetailsDate).add(1, 'days').format(STORE_DATE_FORMAT);

            setDayDetailsDate(dayForward);
        }
    }

    render() {
        const {
            dayDetailsDate,
            expenses,
            closeDayDetails,
            setDayDetailsDate,
            startDate,
            endDate,
        } = this.props;
        const { categories } = this.state;
        const total = expenses.reduce((acc, cur) => acc + cur.total, 0);
        const dayBack = moment(dayDetailsDate).subtract(1, 'days').format(STORE_DATE_FORMAT);
        const dayForward = moment(dayDetailsDate).add(1, 'days').format(STORE_DATE_FORMAT);

        return (
            <Container>
                <Swipe
                    onSwipeMove={this.onSwipeMove}
                    onSwipeEnd={this.onSwipeEnd}
                >
                    <HeaderWrap>
                        <PrevNextDayButton
                            float="left"
                            visible={dayBack >= startDate}
                            onClick={() => setDayDetailsDate(dayBack)}
                        ><ArrowLeft /></PrevNextDayButton>
                        <PrevNextDayButton
                            float="right"
                            visible={dayForward <= endDate}
                            onClick={() => setDayDetailsDate(dayForward)}
                        ><ArrowRight /></PrevNextDayButton>
                        <h3>{ moment(dayDetailsDate).format(HUMAN_MOMENT_DATE_FORMAT) }</h3>
                    </HeaderWrap>
                    <Table>
                        <tbody>
                        {
                            expenses.map(({ category, items, total }) => {
                                const categoryIsOpened = categories[category] && categories[category].opened;

                                return (
                                    <Fragment key={category}>
                                        <Tr onClick={() => this.toggleDetails(category)}>
                                            <Td categoryIsOpened={categoryIsOpened}>
                                                <CategoryColor category={category} /> { category }
                                            </Td>
                                            <RightCol categoryIsOpened={categoryIsOpened}>
                                                { priceFormat(total) }
                                            </RightCol>
                                        </Tr>
                                        {
                                            categoryIsOpened &&
                                            <tr>
                                                <Td colSpan="2" noPad>
                                                    <DayCategoryDetailsTable>
                                                        <tbody>
                                                            {
                                                                items.map((exp, idx) => (
                                                                    <tr key={`${exp.amount}${idx}${exp.description}`}>
                                                                        <DayCategoryDetailsTableColumn>
                                                                            <div style={{ fontSize: '.8em' }}>{ exp.vendor }</div>
                                                                            <div>{ exp.description }</div>
                                                                        </DayCategoryDetailsTableColumn>
                                                                        <DayCategoryDetailsTableRightColumn>
                                                                            { priceFormat(exp.amount) }
                                                                        </DayCategoryDetailsTableRightColumn>
                                                                    </tr>
                                                                ))
                                                            }
                                                        </tbody>
                                                    </DayCategoryDetailsTable>
                                                </Td>
                                            </tr>
                                        }
                                    </Fragment>
                                );
                            })
                        }
                        </tbody>
                    </Table>
                    <h1>{ priceFormat(total) }</h1>
                    <CloseButton onClick={closeDayDetails}>Close</CloseButton>
                </Swipe>
            </Container>
        );
    }
}


export default DayExpensesTable;
