import superHoc from 'Component/superHoc';
import DayExpensesTable from './DayExpensesTable';
import { filteredExpensesByDaySelector } from 'Selector/data/expenses';
import { closeDayDetails, setDayDetailsDate } from 'Action/dayDetails';
import { dayDetailsDateSelector } from 'Selector/dayDetails';
import {
    periodFilterEndDateSelector,
    periodFilterStartDateSelector
} from 'Selector/periodFilter';


const mapStateToProps = (state) => {
    const dayDetailsDate = dayDetailsDateSelector(state);

    return {
        expenses: filteredExpensesByDaySelector(state, dayDetailsDate, true),
        startDate: periodFilterStartDateSelector(state),
        endDate: periodFilterEndDateSelector(state),
        dayDetailsDate,
    };
};
const mapDispatchToProps = {
    closeDayDetails,
    setDayDetailsDate,
};
const connectedDayExpensesTable = superHoc(DayExpensesTable, { mapStateToProps, mapDispatchToProps });


export default connectedDayExpensesTable;
