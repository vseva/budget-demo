import App from './App';
import superHoc from 'Component/superHoc';
import { requestAllData, setDataCode } from 'Action/data';
import { currentSiteSectionSelector } from 'Selector/ui';
import { dataCodeSelector, dataIsLoadedSelector, dataIsLoadingSelector } from 'Selector/data';


const mapStateToProps = state => ({
    currentSection: currentSiteSectionSelector(state),
    dataIsLoading: dataIsLoadingSelector(state),
    dataIsLoaded: dataIsLoadedSelector(state),
    dataCode: dataCodeSelector(state),
});
const mapDispatchToProps = {
    requestAllData,
    setDataCode,
};
const connectApp = superHoc(App, { mapStateToProps, mapDispatchToProps });


export default connectApp;
