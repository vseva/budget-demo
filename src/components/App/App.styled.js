import styled from 'styled-components';


export const Container = styled.div`
    max-width: 800px;
    margin-left: auto;
    margin-right: auto;
    padding: 20px 15px 30px;
`;

export const Error = styled.div`
  padding: 20px;
  border: 1px solid #ff391e;
  color: #ff391e;
  text-align: center;
`;

export const Title = styled.h1`
  font-size: 22px;
  margin-top: 0;
`;

export const CodeContainer = styled.form`
  text-align: center;
`;

export const CodeInput = styled.input`
  display: block;
  width: 100%;
  padding: 10px;
  margin-bottom: 10px;
  font-size: 15px;
  text-align: center;
`;
