import React, { Component, Fragment } from 'react';
import axios from 'axios';
import queryString from 'query-string';
import get from 'lodash/get';

import TopMenu from 'Component/TopMenu';
import Expenses from 'Section/Expenses';
import Income from 'Section/Income';
import Loader from 'Component/Loader';
import DayDetailsModal from 'Component/DayDetailsModal';
import CategoryDetailsModal from 'Component/CategoryDetailsModal';
import VendorDetailsModal from 'Component/VendorDetailsModal';
import NewEntryModal from 'Component/NewEntryModal';
import EditEntryModal from 'Component/EditEntryModal';

import {
    getApiUrl,
} from 'Util';
import {
    EXPENSES_SITE_SECTION,
    INCOME_SITE_SECTION,
} from 'Constant';
import {
    Container,
    Error,
    Title,
    CodeContainer,
    CodeInput,
} from './App.styled';
import Button from 'Style/Button.styled';


const sections = {
    [EXPENSES_SITE_SECTION]: <Expenses />,
    [INCOME_SITE_SECTION]: <Income />,
};


const requestImage = () => {
    axios.get(getApiUrl('recognize')).then(response => {
        console.log(response.data.text.split('\n'));
    });
};

class App extends Component {
    constructor(props) {
        super(props);

        this.getAccess = this.getAccess.bind(this);
        this.submitAccess = this.submitAccess.bind(this);

        this.state = {
            error: '',
            userAuthorized: false,
        };
    }

    componentDidMount() {
        const { code, scope } = queryString.parse(window.location.search) || {};

        if (code && scope) {
            this.submitAccess();
            window.history.replaceState({}, document.title, '/');
        } else {
            this.getAccess();
        }

    }

    getAccess() {
        axios
            .get(getApiUrl('access'))
            .then((response, error) => {
                if (error) return console.error('ERROR 1', error);

                window.location = response.data.url;
            })
            .catch(error => {
                console.error('ERROR 2', error);
            });
    }

    submitAccess() {
        axios
            .get(getApiUrl('verify' + window.location.search))
            .then((response, error) => {
                if (error) {
                    this.setState({
                        error,
                    });

                    return console.error('ERROR 3', error);
                }

                if (response.data.success === true) {
                    return this.setState({
                        userAuthorized: true
                    });
                }
            })
            .catch(error => {
                this.setState({
                    error: get(error, 'response.data.error'),
                });
            });
    }

    render() {
        const {
            currentSection,
            dataIsLoading,
            dataIsLoaded,
            dataCode,
            setDataCode,
            requestAllData,
        } = this.props;
        const {
            error,
            userAuthorized,
        } = this.state;

        if (dataIsLoading) {
            return (
                <Container>
                    <Loader />
                </Container>
            );
        }
        if (error) {
            return (
                <Container>
                    <Error>
                        <Title>{ error.message }</Title>
                        <p>
                            <Button
                                onClick={e => { window.location = '/'; }}
                            >Try to click me</Button>
                        </p>
                    </Error>
                </Container>
            );
        }

        if (!userAuthorized && !dataIsLoaded) return null;
        if (userAuthorized && !dataIsLoaded) return (
            <Container>
                <CodeContainer onSubmit={(e) => { e.preventDefault(); requestAllData(); }}>
                    <CodeInput
                        type="password"
                        value={dataCode}
                        onChange={e => setDataCode(e.target.value)}
                        placeholder="nickname"
                    />
                    <Button type="submit" block>Enter</Button>
                </CodeContainer>
            </Container>
        );

        return (
            <Fragment>
                <TopMenu />
                <Container>
                    { sections[currentSection] }
                </Container>
                <CategoryDetailsModal />
                <DayDetailsModal />
                <VendorDetailsModal />
                <NewEntryModal />
                <EditEntryModal />
            </Fragment>
        );
    }
}


export default App;
