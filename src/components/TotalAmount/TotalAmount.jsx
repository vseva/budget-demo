import React from 'react';
import AnimatedNumber from 'react-animated-number';
import { priceFormat } from 'Util';

import {
    CHART_ANIMATION_DURATION, EXPENSES_COLOR, INCOME_COLOR,
} from 'Constant';
import {
    Container,
    DefiProfi,
    TotalExpenses,
    Table,
    Thead,
    Td,
    CategoryTitle,
} from './TotalAmount.styled';


const TotalAmount = ({
    totalExpensesAmount,
    totalIncomeAmount,
    filteredExpensesAmount,
    filteredIncomeAmount,
}) => {
    const totalDelta = totalIncomeAmount - totalExpensesAmount;
    const filteredDelta = filteredIncomeAmount - filteredExpensesAmount;

    return (
        <Container>
            <Table>
                <Thead>
                    <tr>
                        <Td color={EXPENSES_COLOR}>Expenses</Td>
                        <Td />
                        <Td right color={INCOME_COLOR}>Income</Td>
                    </tr>
                </Thead>
                <tbody>
                    <tr>
                        <Td>
                            <CategoryTitle>Total</CategoryTitle>
                            <TotalExpenses color={EXPENSES_COLOR}>
                                <AnimatedNumber
                                    value={totalExpensesAmount}
                                    formatValue={val => priceFormat(val)}
                                    duration={CHART_ANIMATION_DURATION}
                                    stepPrecision={0}
                                />
                            </TotalExpenses>
                        </Td>
                        <Td centered>
                            <DefiProfi delta={totalDelta}>
                                { priceFormat(totalDelta) }
                            </DefiProfi>
                        </Td>
                        <Td right>
                            <CategoryTitle>Total</CategoryTitle>
                            <TotalExpenses color={INCOME_COLOR}>
                                <AnimatedNumber
                                    value={totalIncomeAmount}
                                    formatValue={val => priceFormat(val)}
                                    duration={CHART_ANIMATION_DURATION}
                                    stepPrecision={0}
                                />
                            </TotalExpenses>
                        </Td>
                    </tr>
                    <tr>
                        <Td>
                            <CategoryTitle>Selected</CategoryTitle>
                            <AnimatedNumber
                                value={filteredExpensesAmount}
                                formatValue={val => priceFormat(val)}
                                duration={CHART_ANIMATION_DURATION}
                                stepPrecision={0}
                            />
                        </Td>
                        <Td centered>
                            <DefiProfi delta={filteredDelta}>
                                { priceFormat(filteredDelta) }
                            </DefiProfi>
                        </Td>
                        <Td right>
                            <CategoryTitle>Selected</CategoryTitle>
                            <AnimatedNumber
                                value={filteredIncomeAmount}
                                formatValue={val => priceFormat(val)}
                                duration={CHART_ANIMATION_DURATION}
                                stepPrecision={0}
                            />
                        </Td>
                    </tr>
                </tbody>
            </Table>
        </Container>
    );
};


export default TotalAmount;
