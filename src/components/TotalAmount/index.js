import superHoc from 'Component/superHoc';
import {
    filteredByDatesTotalIncomeSelector,
    filteredIncomeTotalSelector
} from 'Selector/data/income';
import { currentSiteSectionSelector } from 'Selector/ui';
import {
    filteredByDatesTotalExpensesSelector,
    filteredExpensesTotalSelector,
} from 'Selector/data/expenses';
import TotalAmount from './TotalAmount';


const mapStateToProps = state => ({
    totalExpensesAmount: filteredByDatesTotalExpensesSelector(state),
    totalIncomeAmount: filteredByDatesTotalIncomeSelector(state),
    filteredExpensesAmount: filteredExpensesTotalSelector(state),
    filteredIncomeAmount: filteredIncomeTotalSelector(state),
    currentSiteSection: currentSiteSectionSelector(state),
});
const connectTotalAmount = superHoc(TotalAmount, { mapStateToProps });


export default connectTotalAmount;
