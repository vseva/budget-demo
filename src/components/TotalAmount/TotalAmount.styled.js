import styled from 'styled-components';


export const Container = styled.div`
    letter-spacing: 0;
    margin-bottom: 0;
    margin-top: .1em;
`;

const defiProfiProps = {
    color: ({ delta }) => delta < 0 ? 'red' : 'green',
};

export const DefiProfi = styled.div`
  color: ${defiProfiProps.color};
`;

export const TotalExpenses = styled.div`
  font-size: 18px;
  font-weight: bold;
  color: ${({ color }) => color};
`;

export const Table = styled.table`
  table-layout: fixed;
  border-collapse: collapse;
  width: 100%;
`;

export const Thead = styled.thead`
  font-weight: bold;
  text-transform: uppercase;
`;

const tdProps = {
    textAlign: ({ right, centered }) => {
        if (right) return 'right';
        if (centered) return 'center';
        return 'left';
    },
    color: ({ color }) => color,
};

export const Td = styled.td`
  padding: 5px;
  border-bottom: 1px solid #cfcfcf;
  text-align: ${tdProps.textAlign};
  color: ${tdProps.color};
`;

export const CategoryTitle = styled.div`
  text-transform: uppercase;
  opacity: .7;
  font-size: 11px;
`;
