import superHoc from 'Component/superHoc';
import VendorDetailsModal from './VendorDetailsModal';
import { closeVendorDetails } from 'Action/vendorDetails';
import { vendorDetailsOpenedSelector } from 'Selector/vendorDetails';


const mapStateToProps = state => ({
    vendorDetailsOpened: vendorDetailsOpenedSelector(state),
});
const mapDispatchToProps = {
    closeVendorDetails,
};
const connectVendorDetailsModal = superHoc(VendorDetailsModal, { mapStateToProps, mapDispatchToProps });


export default connectVendorDetailsModal;
