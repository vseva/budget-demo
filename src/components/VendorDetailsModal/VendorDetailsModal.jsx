import React from 'react';
import ReactModal from 'react-modal';

import VendorExpensesTable from 'Component/VendorExpensesTable';


const VendorDetailsModal = ({
    vendorDetailsOpened,
    closeVendorDetails,
}) => {
    if (!vendorDetailsOpened) return null;

    return (
        <ReactModal
            isOpen={vendorDetailsOpened}
            contentLabel="Vendor Expenses Details"
            onRequestClose={closeVendorDetails}
        >
            <VendorExpensesTable />
        </ReactModal>
    )
};


export default VendorDetailsModal;
