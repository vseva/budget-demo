import superHoc from 'Component/superHoc';
import DayDetailsModal from './DayDetailsModal';
import { dayDetailsOpenedSelector } from 'Selector/dayDetails';
import { closeDayDetails, openDayDetails } from 'Action/dayDetails';


const mapStateToProps = state => ({
    dayDetailsOpened: dayDetailsOpenedSelector(state),
});
const mapDispatchToProps = {
    openDayDetails,
    closeDayDetails,
};
const connectDayDetailsModal = superHoc(DayDetailsModal, { mapStateToProps, mapDispatchToProps });


export default connectDayDetailsModal;
