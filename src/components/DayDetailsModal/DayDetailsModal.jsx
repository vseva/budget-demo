import React from 'react';
import ReactModal from 'react-modal';

import DayExpensesTable from 'Component/DayExpensesTable';


const DayDetailsModal = ({
    dayDetailsOpened,
    closeDayDetails,
}) => {
    if (!dayDetailsOpened) return null;

    return (
        <ReactModal
            isOpen={dayDetailsOpened}
            contentLabel="Day Expenses Details"
            onRequestClose={closeDayDetails}
        >
            <DayExpensesTable />
        </ReactModal>
    )
};


export default DayDetailsModal;
