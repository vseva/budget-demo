import styled from 'styled-components';

import { getCategoryColor } from 'Util';


export const Container = styled.div`
  padding-bottom: 100px;
  position: relative;
`;

export const CloseButton = styled.button`
  border: 1px solid black;
  border-radius: 0;
  background-color: transparent;
  outline: 0;
  text-align: center;
  text-transform: uppercase;
  cursor: pointer;
  right: 30px;
  width: calc(100% - 60px);
  padding: 20px;
  letter-spacing: 2px;
  font-size: 14px;
  position: absolute;
  bottom: 30px;
`;

export const Table = styled.table`
  width: 100%;
  border-collapse: collapse;
`;

export const Tr = styled.tr`
`;

const categoryProps = {
    backgroundColor: ({ categoryIsOpened }) => (categoryIsOpened ? 'rgba(18, 140, 255, .3)' : 'transparent'),
};

const tdProps = {
    padding: ({ noPad }) => (noPad ? 0 : '10px'),
};

export const Td = styled.td`
  padding: ${tdProps.padding};
  border-bottom: 1px solid #d6d6d6;
  background-color: ${categoryProps.backgroundColor};
`;

export const RightCol = styled.td`
  padding: 10px;
  border-bottom: 1px solid #d6d6d6;
  font-weight: bold;
  text-align: right;
  background-color: ${categoryProps.backgroundColor};
`;

export const HeaderWrap = styled.div`
  &:after {
    content: '';
    display: table;
    clear: both;
  }
`;

export const CategoryColor = styled.span`
  display: inline-block;
  width: 13px;
  height: 13px;
  border-radius: 100%;
  position: relative;
  top: 1px;
  margin-right: 4px;
  background-color: ${ ({ category }) => getCategoryColor(category) };
`;
