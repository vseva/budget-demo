import superHoc from 'Component/superHoc';
import CategoryExpensesTable from './CategoryExpensesTable';
import { filteredExpensesByCategorySelector } from 'Selector/data/expenses';
import {
    periodFilterEndDateSelector,
    periodFilterStartDateSelector
} from 'Selector/periodFilter';
import { categoryDetailsCategorySelector } from 'Selector/categoryDetails';
import { closeCategoryDetails } from 'Action/categoryDetails';


const mapStateToProps = (state) => {
    const category = categoryDetailsCategorySelector(state);

    return {
        expenses: filteredExpensesByCategorySelector(state, category, true),
        startDate: periodFilterStartDateSelector(state),
        endDate: periodFilterEndDateSelector(state),
        category,
    };
};
const mapDispatchToProps = {
    closeCategoryDetails,
};
const connectedCategoryExpensesTable = superHoc(CategoryExpensesTable, { mapStateToProps, mapDispatchToProps });


export default connectedCategoryExpensesTable;
