import React, { Component, Fragment } from 'react';
import orderBy from 'lodash/orderBy';

import { displayHumanDateFormat, humanDateFormat, priceFormat } from 'Util';
import {
    Container,
    Table,
    Tr,
    Td,
    RightCol,
    CloseButton,
    HeaderWrap,
    CategoryColor,
} from './CategoryExpensesTable.styled';


class CategoryExpensesTable extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const {
            category,
            expenses,
            closeCategoryDetails,
        } = this.props;
        const total = expenses.reduce((acc, cur) => acc + cur.amount, 0);

        return (
            <Container>
                <HeaderWrap>
                    <h3><CategoryColor category={category} /> { category }</h3>
                </HeaderWrap>
                <Table>
                    <tbody>
                    {
                        orderBy(expenses, 'amount', 'desc').map(({ description, amount, date }, idx) => (
                            <Fragment key={`${category}${idx}`}>
                                <Tr key={description}>
                                    <Td>
                                        <b>{ displayHumanDateFormat(date) }</b>
                                    </Td>
                                    <Td>
                                        { description }
                                    </Td>
                                    <RightCol>
                                        { priceFormat(amount) }
                                    </RightCol>
                                </Tr>
                            </Fragment>
                        ))
                    }
                    </tbody>
                </Table>
                <h1>{ priceFormat(total) }</h1>
                <CloseButton onClick={closeCategoryDetails}>Close</CloseButton>
            </Container>
        );
    }
}


export default CategoryExpensesTable;
