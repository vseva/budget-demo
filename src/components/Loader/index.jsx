import React from 'react';

import {
    Container
} from './Loader.styled';


const Loader = () => (
    <Container>
        <div className="lds-ripple">
            <div/>
            <div/>
        </div>
    </Container>
);


export default Loader;
