import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';


const superHoc = (Component, params = {}) => {
    const { mapStateToProps = null, mapDispatchToProps = null } = params;
    const isConnected = mapStateToProps || mapDispatchToProps;

    if (isConnected) {
        const dispatchMapper = !mapDispatchToProps ?
            null :
            dispatch => bindActionCreators(mapDispatchToProps, dispatch);

        return connect(mapStateToProps, dispatchMapper)(Component);
    }

    return Component;
};


export default superHoc;
