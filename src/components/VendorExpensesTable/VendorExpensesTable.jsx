import React, { Component, Fragment } from 'react';
import orderBy from 'lodash/orderBy';

import { displayHumanDateFormat, priceFormat } from 'Util';
import {
    Container,
    Table,
    Tr,
    Td,
    RightCol,
    CloseButton,
    HeaderWrap,
} from './VendorExpensesTable.styled';


class VendorExpensesTable extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const {
            vendor,
            data,
            closeVendorDetails,
        } = this.props;
        const total = data.reduce((acc, cur) => acc + cur.amount, 0);

        return (
            <Container>
                <HeaderWrap>
                    <h3>{ vendor }</h3>
                </HeaderWrap>
                <Table>
                    <tbody>
                    {
                        orderBy(data, 'amount', 'desc').map(({ description, amount, date }, idx) => (
                            <Fragment key={`${vendor}${idx}`}>
                                <Tr key={description}>
                                    <Td>
                                        <b>{ displayHumanDateFormat(date) }</b>
                                    </Td>
                                    <Td>
                                        { description }
                                    </Td>
                                    <RightCol>
                                        { priceFormat(amount) }
                                    </RightCol>
                                </Tr>
                            </Fragment>
                        ))
                    }
                    </tbody>
                </Table>
                <h1>{ priceFormat(total) }</h1>
                <CloseButton onClick={closeVendorDetails}>Close</CloseButton>
            </Container>
        );
    }
}


export default VendorExpensesTable;
