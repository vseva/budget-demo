import superHoc from 'Component/superHoc';
import VendorExpensesTable from './VendorExpensesTable';
import { closeVendorDetails } from 'Action/vendorDetails';
import { filteredExpensesByVendorSelector } from 'Selector/data/expenses';
import { vendorDetailsVendorSelector } from 'Selector/vendorDetails';

const mapStateToProps = (state) => {
    const vendor = vendorDetailsVendorSelector(state);

    return {
        data: filteredExpensesByVendorSelector(state, vendor),
        vendor,
    };
};
const mapDispatchToProps = {
    closeVendorDetails,
};
const connectedVendorExpensesTable = superHoc(VendorExpensesTable, { mapStateToProps, mapDispatchToProps });


export default connectedVendorExpensesTable;
