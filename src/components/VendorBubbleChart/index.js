import superHoc from 'Component/superHoc';
import VendorBubbleChart from './VendorBubbleChart';
import { filteredExpensesGroupedByVendorSelector } from 'Selector/data/expenses';
import { openVendorDetails, setVendorDetailsVendor } from 'Action/vendorDetails';


const mapStateToProps = state => ({
    data: filteredExpensesGroupedByVendorSelector(state),
});
const mapDispatchToProps = {
    setVendorDetailsVendor,
    openVendorDetails,
};


export default superHoc(VendorBubbleChart, { mapStateToProps, mapDispatchToProps });
