import React from 'react';
import BubbleChart from 'Component/ReactD3BubbleChart';

import { Container } from './VendorBubbleChart.styled';


const VendorBubbleChart = ({ data, setVendorDetailsVendor, openVendorDetails }) => {
    const graphData = Object.keys(data).reduce((acc, cur) => acc.concat({
        label: cur,
        value: data[cur].reduce((vAcc, vCur) => vAcc += vCur.amount, 0),
    }), []);

    const bubbleClickFun = vendor => {
        setVendorDetailsVendor(vendor);
        openVendorDetails();
    };

    return (
        <Container>
            <BubbleChart
                graph={{
                    offsetX: 0,
                    offsetY: 0,
                }}
                valueFont={{
                    family: 'Arial',
                    size: 12,
                    color: '#fff',
                    weight: 'bold',
                }}
                labelFont={{
                    family: 'Arial',
                    size: 16,
                    color: '#fff',
                    weight: 'bold',
                }}
                bubbleClickFun={bubbleClickFun}
                data={graphData}
            />
        </Container>
    )
};


export default VendorBubbleChart;
