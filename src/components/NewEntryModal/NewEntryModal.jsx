import React from 'react';
import ReactModal from 'react-modal';

import EntryForm from 'Component/EntryForm';
import CloseIcon from 'Image/CloseIcon';
import {
    ModalHeader,
    ModalHeaderTitle,
    ModalCloseButtonWrap,
} from 'Style/Modal.styled';


const NewEntryModal = ({
    newEntryModalOpened,
    closeNewEntryModal,
}) => {
    if (!newEntryModalOpened) return null;

    return (
        <ReactModal
            isOpen={newEntryModalOpened}
            contentLabel="Add New Entry Form"
            onRequestClose={closeNewEntryModal}
        >
            <ModalHeader>
                <ModalHeaderTitle>Add</ModalHeaderTitle>
                <ModalCloseButtonWrap onClick={closeNewEntryModal}>
                    <CloseIcon />
                </ModalCloseButtonWrap>
            </ModalHeader>
            <EntryForm />
        </ReactModal>
    )
};


export default NewEntryModal;
