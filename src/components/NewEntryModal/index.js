import superHoc from 'Component/superHoc';
import NewEntryModal from './NewEntryModal';
import { newEntryModalOpenedSelector } from 'Selector/newEntry';
import { closeNewEntryModal } from 'Action/newEntry';


const mapStateToProps = state => ({
    newEntryModalOpened: newEntryModalOpenedSelector(state),
});
const mapDispatchToProps = {
    closeNewEntryModal,
};
const connectNewEntryModal = superHoc(NewEntryModal, { mapStateToProps, mapDispatchToProps });


export default connectNewEntryModal;
