import superHoc from 'Component/superHoc';
import CategoriesFilter from './CategoriesFilter';
import {
    checkAllExpensesCategories,
    toggleExpensesCategory,
    uncheckAllExpensesCategories
} from 'Action/expensesCategoriesFilter';
import { expensesCategoriesFilterSelector } from 'Selector/expensesCategoriesFilter';
import {
    currentSiteSectionSelector,
    expensesCategoriesFilterOpenedSelector,
    incomeCategoriesFilterOpenedSelector
} from 'Selector/ui';
import { EXPENSES_CATEGORIES_FILTER } from 'Constant';
import { incomeCategoriesFilterSelector } from 'Selector/incomeCategoriesFilter';
import {
    checkAllIncomeCategories,
    toggleIncomeCategory,
    uncheckAllIncomeCategories
} from 'Action/incomeCategoriesFilter';
import { setSiteSection, toggleExpensesCategoriesFilter, toggleIncomeCategoriesFilter } from 'Action/ui';


const mapStateToProps = (state, { type }) => {
    const expensesFilter = type === EXPENSES_CATEGORIES_FILTER;
    const categoriesSelector = expensesFilter ?
        expensesCategoriesFilterSelector :
        incomeCategoriesFilterSelector;
    const categoriesFilterOpened = expensesFilter ?
        expensesCategoriesFilterOpenedSelector :
        incomeCategoriesFilterOpenedSelector;

    return {
        categories: categoriesSelector(state),
        categoriesFilterOpened: categoriesFilterOpened(state),
        currentSiteSection: currentSiteSectionSelector(state),
    };
};
const mapDispatchToProps = {
    toggleExpensesCategory,
    toggleIncomeCategory,
    toggleIncomeCategoriesFilter,
    toggleExpensesCategoriesFilter,
    checkAllExpensesCategories,
    uncheckAllExpensesCategories,
    checkAllIncomeCategories,
    uncheckAllIncomeCategories,
    setSiteSection,
};
const connectCategoriesFilter = superHoc(CategoriesFilter, { mapStateToProps, mapDispatchToProps });


export default connectCategoriesFilter;
