import styled from 'styled-components';
import { EXPENSES_CATEGORIES_FILTER, EXPENSES_COLOR, INCOME_COLOR } from 'Constant';


const containerProps = {
    padding: ({ categoriesFilterOpened }) => (categoriesFilterOpened ? '0 15px 15px' : 0),
};

const toggleButtonProps = {
    color: ({ type }) => (type === EXPENSES_CATEGORIES_FILTER ? EXPENSES_COLOR : INCOME_COLOR),
};

export const Container = styled.div`
  margin-bottom: 20px;
`;

const buttonProps = {
    borderBottomColor: ({ checked, color }) => (checked ? color : 'transparent'),
    opacity: ({ checked }) => (checked ? 1 : .7),
};

export const Button = styled.label`
  display: inline-block;
  margin: 0 13px 5px 0;
  font-size: 13px;
  cursor: pointer;
  border-bottom-width: 3px;
  border-bottom-style: solid;
  border-bottom-color: ${buttonProps.borderBottomColor};
  transition: opacity .2s ease;
  opacity: ${buttonProps.opacity};
  padding: ${containerProps.padding};
  
  input {
    margin-right: 2px;
  }
`;

export const ToggleButton = styled.span`
  cursor: pointer;
  color: ${toggleButtonProps.color};
  text-transform: uppercase;
  font-weight: bold;
  font-size: 16px;
  letter-spacing: 2px;
`;

const toggleButtonWrapProps = {
    padding: ({ categoriesFilterOpened }) => (categoriesFilterOpened ? '18px 0 4px' : '11px 15px'),
};

export const ToggleButtonWrap = styled.div`
  border: 1px solid ${toggleButtonProps.color};
  cursor: pointer;
  text-align: center;
  padding: ${toggleButtonWrapProps.padding};
  user-select: none;
  margin: 0 60px;
`;

export const ToggleAllButtonsWrap = styled.div`
  text-align: center;
  margin-top: 10px;
`;

export const CategoriesWrapper = styled.div`
  margin-top: 10px;
  margin-bottom: -5px;
`;

export const ToggleAllCategoriesButton = styled.span`
  display: inline-block;
  margin: 0 10px;
  text-transform: uppercase;
  border-bottom: 1px dotted;
  cursor: pointer;
  font-size: 11px;
  
  &:hover {
    border-bottom-color: transparent;
  }
`;

export const Input = styled.input`
  display: none;
`;

const sectionButtonsProps = {
    cursor: ({ isActive }) => (isActive ? 'default' : 'pointer'),
    opacity: ({ isActive }) => (isActive ? 1 : 0.5),
    float: ({ float }) => float,
};

export const ExpensesIconWrap = styled.div`
  cursor: ${sectionButtonsProps.cursor};
  opacity: ${sectionButtonsProps.opacity};
  float: ${sectionButtonsProps.float};
  width: 31px;
  height: 31px;
  position: relative;
  margin-right: 4px;
  top: 5px;
  
  svg {
    width: 100%;
    height: 100%;
  }
  
  &:hover {
    opacity: 1 !important;
  }
`;

export const IncomeIconWrap = styled.div`
  cursor: ${sectionButtonsProps.cursor};
  opacity: ${sectionButtonsProps.opacity};
  float: ${sectionButtonsProps.float};
  width: 31px;
  height: 31px;
  position: relative;
  top: 5px;
  
  svg {
    width: 100%;
    height: 100%;
  }
`;
