import React, { Fragment } from 'react';

import ExpensesIcon from 'Image/ExpensesIcon';
import IncomeIcon from 'Image/IncomeIcon';
import { EXPENSES_SITE_SECTION, INCOME_SITE_SECTION } from 'Constant';
import {
    Button,
    Input,
    Container,
    ToggleButtonWrap,
    ToggleButton,
    CategoriesWrapper,
    ToggleAllButtonsWrap,
    ToggleAllCategoriesButton,
    IncomeIconWrap,
    ExpensesIconWrap,
} from './CategoriesFilter.styled';
import { EXPENSES_CATEGORIES_FILTER } from 'Constant';
import { getCategoriesFilterTitle } from 'Util';
import { setSiteSection } from 'Action/ui';


const CategoriesFilter = ({
    categoriesFilterOpened,
    toggleExpensesCategory,
    toggleIncomeCategory,
    toggleIncomeCategoriesFilter,
    toggleExpensesCategoriesFilter,
    checkAllExpensesCategories,
    uncheckAllExpensesCategories,
    checkAllIncomeCategories,
    uncheckAllIncomeCategories,
    categories,
    type,
    currentSiteSection,
    setSiteSection,
}) => {
    const isExpensesFilter = type === EXPENSES_CATEGORIES_FILTER;
    const toggleCategoryAction = isExpensesFilter ?
        toggleExpensesCategory :
        toggleIncomeCategory;

    const toggleCategory = (e) => {
        const category = e.target.value;
        const checked = e.target.checked;

        toggleCategoryAction({
            category,
            checked,
        });
    };

    const toggleFilterAction = isExpensesFilter ?
        toggleExpensesCategoriesFilter :
        toggleIncomeCategoriesFilter;

    const checkAll = isExpensesFilter ?
        checkAllExpensesCategories :
        checkAllIncomeCategories;

    const uncheckAll = isExpensesFilter ?
        uncheckAllExpensesCategories :
        uncheckAllIncomeCategories;

    if (!categories.length) return null;

    const checkedCategories = categories.filter(category => category.checked);

    return (
        <Container categoriesFilterOpened={categoriesFilterOpened} type={type}>
            <ExpensesIconWrap
                float="left"
                isActive={currentSiteSection === EXPENSES_SITE_SECTION}
                onClick={() => { if (currentSiteSection !== EXPENSES_SITE_SECTION ) setSiteSection(EXPENSES_SITE_SECTION)}}
            >
                <ExpensesIcon />
            </ExpensesIconWrap>
            <IncomeIconWrap
                float="right"
                isActive={currentSiteSection === INCOME_SITE_SECTION}
                onClick={() => { if (currentSiteSection !== INCOME_SITE_SECTION ) setSiteSection(INCOME_SITE_SECTION)}}
            >
                <IncomeIcon />
            </IncomeIconWrap>
            <ToggleButtonWrap onClick={toggleFilterAction} type={type}>
                <ToggleButton type={type}>
                    { getCategoriesFilterTitle(type) }
                </ToggleButton>
            </ToggleButtonWrap>
            {
                categoriesFilterOpened &&
                <Fragment>
                    <ToggleAllButtonsWrap>
                        {
                            !!checkedCategories.length &&
                            <ToggleAllCategoriesButton
                                onClick={uncheckAll}
                            >
                                Uncheck All
                            </ToggleAllCategoriesButton>
                        }
                        {
                            checkedCategories.length !== categories.length &&
                            <ToggleAllCategoriesButton
                                onClick={checkAll}
                            >
                                Check All
                            </ToggleAllCategoriesButton>
                        }
                    </ToggleAllButtonsWrap>
                    <CategoriesWrapper>
                        {
                            categories.map((item, idx) => {
                                const name = item.category;

                                return (
                                    <Button { ...item } key={`${name}${idx}`}>
                                        <Input
                                            type="checkbox"
                                            checked={item.checked}
                                            value={name}
                                            onChange={toggleCategory}
                                        />
                                        { name }
                                    </Button>
                                );
                            })
                        }
                    </CategoriesWrapper>
                </Fragment>
            }
        </Container>
    );
};


export default CategoriesFilter;
