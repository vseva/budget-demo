import React from 'react';

import PeriodFilter from 'Component/PeriodFilter';
import PlusIcon from 'Image/PlusIcon';
import RefreshIcon from 'Image/RefreshIcon';

import {
    Container,
    PlusIconWrap,
    RefreshIconWrap,
} from './TopMenu.styled';


const TopMenu = ({
    openNewEntryModal,
    requestAllData,
}) => (
    <Container>
        <RefreshIconWrap onClick={requestAllData}>
            <RefreshIcon />
        </RefreshIconWrap>
        <PlusIconWrap onClick={openNewEntryModal}>
            <PlusIcon />
        </PlusIconWrap>
        <PeriodFilter />
    </Container>
);


export default TopMenu;
