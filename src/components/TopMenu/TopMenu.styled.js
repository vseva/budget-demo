import styled from 'styled-components';


export const Container = styled.div`
  max-width: 800px;
  margin-left: auto;
  margin-right: auto;
  padding: 15px 15px 0;
`;

export const PlusIconWrap = styled.div`
  cursor: pointer;
  width: 31px;
  height: 31px;
  float: right;
  position: relative;
  top: -1px;

  
  svg {
    width: 100%;
    height: 100%;
  }
`;

export const RefreshIconWrap = styled.div`
  cursor: pointer;
  width: 31px;
  height: 31px;
  float: left;
  position: relative;
  top: -1px;
  
  svg {
    width: 100%;
    height: 100%;
  }
`;
