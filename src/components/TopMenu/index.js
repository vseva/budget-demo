import superHoc from 'Component/superHoc';
import TopMenu from './TopMenu';
import { openNewEntryModal } from 'Action/newEntry';
import { requestAllData } from 'Action/data';


const mapStateToProps = state => ({
});
const mapDispatchToProps = {
    openNewEntryModal,
    requestAllData,
};


export default superHoc(TopMenu, { mapDispatchToProps, mapStateToProps });
