import superHoc from 'Component/superHoc';
import { filteredExpensesSelector } from 'Selector/data/expenses';
import FullTable from './FullTable';
import { EXPENSES_FULL_TABLE } from 'Constant';
import { filteredIncomeSelector } from 'Selector/data/income';


const mapStateToProps = (state, { type }) => {
    const dataSelector = type === EXPENSES_FULL_TABLE ?
        filteredExpensesSelector :
        filteredIncomeSelector;

    return {
        data: dataSelector(state),
    };
};
const connectFullTable = superHoc(FullTable, { mapStateToProps });


export default connectFullTable;
