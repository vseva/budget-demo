import React from 'react';
import ReactTable from 'react-table';
import moment from 'moment';
import includes from 'lodash/includes';
import 'react-table/react-table.css';

import { priceFormat } from 'Util';
import {
    DISPLAY_MOMENT_DATE_FORMAT,
    EXPENSE_ENTRY_TYPE,
    EXPENSES_FULL_TABLE,
    INCOME_ENTRY_TYPE,
} from 'Constant';
import FullTableCell from 'Component/FullTableCell';
import { Container } from './FullTable.styled';


const columns = (type) => {
    const entryType = type === EXPENSES_FULL_TABLE ?
        EXPENSE_ENTRY_TYPE :
        INCOME_ENTRY_TYPE;
    const commonCell = cellInfo => (
        <FullTableCell entryType={entryType} cellInfo={cellInfo} value={cellInfo.value} />
    );

    const topCols = [
        {
            id: 'date',
            Header: 'Date',
            accessor: d => moment(d.date).format(DISPLAY_MOMENT_DATE_FORMAT),
            Cell: commonCell,
        },
        {
            Header: 'Description',
            accessor: 'description',
            Cell: commonCell,
        },
    ];

    const bottomCols = [
        {
            Header: 'Category',
            accessor: 'category',
            Cell: commonCell,
        },
        {
            Header: 'Amount, ₽',
            accessor: 'amount',
            Cell: cellInfo => (
                <FullTableCell
                    cellInfo={cellInfo}
                    value={priceFormat(cellInfo.original.amount, { withCurrency: false })}
                    entryType={entryType}
                />
            ),
        },
    ];

    if (type === EXPENSES_FULL_TABLE) {
        return topCols.concat({
            Header: 'Vendor',
            accessor: 'vendor',
            Cell: commonCell,
        }).concat(bottomCols);
    }

    return topCols.concat(bottomCols);
};

const FullTable = ({ data, type }) => {
    if (!data.length) return null;

    return (
        <Container>
            <ReactTable
                data={data}
                columns={columns(type)}
                defaultSorted={[
                    {
                        id: 'date',
                        asc: true
                    }
                ]}
                pageSize={Math.min(40, data.length)}
                defaultFilterMethod={(filter, row) => includes(row[filter.id].toLowerCase(), filter.value.toLowerCase())}
                filterable
            />
        </Container>
    );
};


export default FullTable;
