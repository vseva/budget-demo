import React, { Component } from 'react';
import Datepicker from 'react-datepicker';
import moment from 'moment';
import Creatable from 'react-select/lib/Creatable';
import includes from 'lodash/includes';

import {
    FormGroup,
    FormGroupTextInput,
    RadioLabel,
} from './EntryForm.styled';
import Button from 'Style/Button.styled';
import { DATE_FORMAT, EXPENSE_ENTRY_TYPE, INCOME_ENTRY_TYPE, STORE_DATE_FORMAT } from 'Constant';


const formSuggests = {
    [EXPENSE_ENTRY_TYPE]: [
        {
            label: 'Description',
            name: 'description',
        },
        {
            label: 'Vendor',
            name: 'vendor',
        },
        {
            label: 'Category',
            name: 'category',
        }
    ],
    [INCOME_ENTRY_TYPE]: [
        {
            label: 'Description',
            name: 'description',
        },
        {
            label: 'Category',
            name: 'category',
        }
    ]
};

class DateInput extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const { value, onClick } = this.props;

        return (
            <FormGroupTextInput
                type="text"
                placeholder="Date"
                name="date"
                value={value}
                onClick={onClick}
                onChange={() => {}}
                autocomplete="new-password"
                readOnly
            />
        );
    }
}

class EntryForm extends Component {
    constructor(props) {
        super(props);

        this.submit = this.submit.bind(this);
        this.onInputChange = this.onInputChange.bind(this);
        this.onDateChange = this.onDateChange.bind(this);
        this.formIsValid = this.formIsValid.bind(this);
        this.onSuggestChange = this.onSuggestChange.bind(this);

        const {
            edit,
            entryType,
            updateNewEntryFormField,
            updateEditEntryFormField,
            updateExpensesSuggestLists,
            updateIncomeSuggestLists,
        } = props;

        this.fieldUpdater = edit ? updateEditEntryFormField : updateNewEntryFormField;
        this.suggestUpdater = entryType === EXPENSE_ENTRY_TYPE ?
            updateExpensesSuggestLists :
            updateIncomeSuggestLists;
        this.onEntryTypeChange = this.onEntryTypeChange.bind(this);
    }

    formIsValid() {
        const {
            formData,
            entryType,
        } = this.props;
        const errors = [];
        const columns = [
            'date',
            'amount',
            'description',
            'category',
        ];

        columns.forEach(col => {
            if (!formData[col]) {
                errors.push('error');
            }
        });

        if (entryType === EXPENSE_ENTRY_TYPE && !formData.vendor) {
            errors.push('error');
        }

        return !errors.length;
    }

    submit(e) {
        e.preventDefault();

        const {
            formData,
            newEntry,
            editEntry,
            edit,
            entryType,
        } = this.props;

        if (!this.formIsValid()) {
            return alert('Please, fill all fields');
        }

        const data = {
            rowData: { ...formData, amount: Number(formData.amount) },
            entryType,
        };

        if (edit) {
            return editEntry(data);
        }

        return newEntry(data);
    }

    onInputChange({ target }) {
        const { name, value } = target;

        this.fieldUpdater(name, value);
    }

    onDateChange(date) {
        this.fieldUpdater('date', moment(date).format(STORE_DATE_FORMAT));
    }

    onSuggestChange(name, { value }) {
        const {
            suggestLists,
        } = this.props;
        const values = suggestLists[name] || [];

        if (!includes(values, value)) {
            this.suggestUpdater(name, value);
        }

        this.fieldUpdater(name, value);
    }

    onEntryTypeChange(e) {
        this.props.setEntryType(e.target.value);
    }

    render() {
        const {
            formData,
            suggestLists,
            requestInProgress,
            entryType,
            edit
        } = this.props;

        return (
            <div>
                <form onSubmit={this.submit}>
                    {
                        !edit &&
                        <FormGroup>
                            <RadioLabel>
                                <input
                                    type="radio"
                                    name="entryType"
                                    value={EXPENSE_ENTRY_TYPE}
                                    checked={entryType === EXPENSE_ENTRY_TYPE}
                                    onChange={this.onEntryTypeChange}
                                />
                                Expense
                            </RadioLabel>
                            <RadioLabel>
                                <input
                                    type="radio"
                                    name="entryType"
                                    value={INCOME_ENTRY_TYPE}
                                    checked={entryType === INCOME_ENTRY_TYPE}
                                    onChange={this.onEntryTypeChange}
                                />
                                Income
                            </RadioLabel>
                        </FormGroup>
                    }
                    <FormGroup>
                        <Datepicker
                            customInput={<DateInput />}
                            selected={new Date(formData.date)}
                            onChange={this.onDateChange}
                            dateFormat={DATE_FORMAT}
                            maxDate={new Date()}
                        />
                    </FormGroup>
                    <FormGroup>
                        <FormGroupTextInput
                            type="number"
                            pattern="[0-9]*"
                            placeholder="Amount"
                            name="amount"
                            value={formData.amount || ''}
                            onChange={this.onInputChange}
                            required
                        />
                    </FormGroup>
                    {
                        formSuggests[entryType].map(({ label, name }) => {
                            const optionsList = (suggestLists[name] || []).map(d => ({ label: d, value: d }));
                            const value = formData[name];

                            return (
                                <FormGroup key={name}>
                                    <Creatable
                                        placeholder={label}
                                        options={optionsList}
                                        value={value ? { label: value, value } : ''}
                                        onChange={option => this.onSuggestChange(name, option)}
                                    />
                                </FormGroup>
                            );
                        })
                    }
                    <Button
                        type="submit"
                        disabled={requestInProgress}
                        block
                    >Save</Button>
                </form>
            </div>
        );
    }
}


export default EntryForm;
