import styled from 'styled-components';


export const FormGroup = styled.div`
  margin-bottom: 10px;
  
  .react-datepicker-wrapper {
    display: block;
  }
  
  .react-datepicker__input-container {
    display: block;
  }
`;

export const FormGroupTextInput = styled.input`
  width: 100%;
  padding: 10px;
  font-size: 14px;
`;

export const RadioLabel = styled.label`
  margin-right: 10px;
  cursor: pointer;
  
  input {
    margin-right: 5px;
  }
`;
