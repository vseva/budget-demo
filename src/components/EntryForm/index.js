import superHoc from 'Component/superHoc';
import { newEntry, updateNewEntryFormField } from 'Action/newEntry';
import EntryForm from './EntryForm';
import { expensesSuggestListsSelector } from 'Selector/data/expenses';
import { newEntryFormDataSelector, newEntryRequestInProgressSelector, newEntryTypeSelector } from 'Selector/newEntry';
import { updateExpensesSuggestLists, updateIncomeSuggestLists } from 'Action/data';
import { editEntryFormDataSelector, editEntryRequestInProgressSelector } from 'Selector/editEntry';
import { editEntry, updateEditEntryFormField } from 'Action/editEntry';
import { setEntryType } from 'Action';
import { EXPENSE_ENTRY_TYPE } from 'Constant';
import { incomeSuggestListsSelector } from 'Selector/data/income';


const mapStateToProps = (state, { edit }) => {
    const formDataSelector = edit ?
        editEntryFormDataSelector :
        newEntryFormDataSelector;

    const requestInProgressSelector = edit ?
        editEntryRequestInProgressSelector :
        newEntryRequestInProgressSelector;

    const entryType = newEntryTypeSelector(state);

    const suggestListsSelector = entryType === EXPENSE_ENTRY_TYPE ?
        expensesSuggestListsSelector :
        incomeSuggestListsSelector;

    return {
        formData: formDataSelector(state),
        requestInProgress: requestInProgressSelector(state),
        suggestLists: suggestListsSelector(state),
        entryType,
    };
};
const mapDispatchToProps = {
    newEntry,
    editEntry,
    updateNewEntryFormField,
    updateEditEntryFormField,
    updateExpensesSuggestLists,
    updateIncomeSuggestLists,
    setEntryType,
};
const connectEntryForm = superHoc(EntryForm, { mapStateToProps, mapDispatchToProps });


export default connectEntryForm;
