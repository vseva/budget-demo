import superHoc from 'Component/superHoc';
import EditEntryModal from './EditEntryModal';
import { editEntryModalOpenedSelector } from 'Selector/editEntry';
import { closeEditEntryModal } from 'Action/editEntry';


const mapStateToProps = state => ({
    editEntryModalOpened: editEntryModalOpenedSelector(state),
});
const mapDispatchToProps = {
    closeEditEntryModal,
};
const connectEditEntryModal = superHoc(EditEntryModal, { mapStateToProps, mapDispatchToProps });


export default connectEditEntryModal;
