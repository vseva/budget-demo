import React from 'react';
import ReactModal from 'react-modal';

import EntryForm from 'Component/EntryForm';
import CloseIcon from 'Image/CloseIcon';
import {
    ModalHeader,
    ModalHeaderTitle,
    ModalCloseButtonWrap,
} from 'Style/Modal.styled';


const EditEntryModal = ({
    editEntryModalOpened,
    closeEditEntryModal,
}) => {
    if (!editEntryModalOpened) return null;

    return (
        <ReactModal
            isOpen={editEntryModalOpened}
            contentLabel="Edit Entry Form"
            onRequestClose={closeEditEntryModal}
        >
            <ModalHeader>
                <ModalHeaderTitle>Edit</ModalHeaderTitle>
                <ModalCloseButtonWrap onClick={closeEditEntryModal}>
                    <CloseIcon />
                </ModalCloseButtonWrap>
            </ModalHeader>
            <EntryForm edit />
        </ReactModal>
    )
};


export default EditEntryModal;
