import superHoc from 'Component/superHoc';
import FullTableCell from './FullTableCell';
import { openEditEntry } from 'Action/editEntry';


const mapDispatchToProps = {
    openEditEntry
};
const connectedFullTableCell = superHoc(FullTableCell, { mapDispatchToProps });


export default connectedFullTableCell;
