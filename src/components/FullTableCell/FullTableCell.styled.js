import styled from 'styled-components';


export const EditButton = styled.button`
  -webkit-appearance: none;
  border: none;
  outline: none;
  visibility: hidden;
  display: inline-block;
  vertical-align: baseline;
  padding: 0;
  cursor: pointer;
  width: 14px;
  height: 14px;
  margin-left: 7px;
  position: absolute;
  top: 0;
  right: 3px;
  opacity: 0;
  transition: opacity .2s ease;
  
  svg {
    width: 100%;
    height: 100%;
  }
  
  &:hover {
    opacity: 1 !important;
  }
`;

export const Container = styled.div`
  position: relative;

  &:hover {
    ${EditButton} {
      visibility: visible;
      opacity: .5;
    }
  }
`;
