import React from 'react';

import EditIcon from 'Image/EditIcon';
import {
    Container,
    EditButton,
} from './FullTableCell.styled';


const FullTableCell = ({ cellInfo, value, openEditEntry, entryType }) => {
   return (
       <Container>
           { value }
           <EditButton onClick={() => openEditEntry(cellInfo.original, entryType)}>
               <EditIcon />
           </EditButton>
       </Container>
   );
};


export default FullTableCell;
