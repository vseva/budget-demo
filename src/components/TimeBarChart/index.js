import superHoc from 'Component/superHoc';
import { filteredExpensesForTimeBarChart } from 'Selector/data/expenses';
import TimeBarChart from './TimeBarChart';
import {
    checkedExpensesCategoriesWithColorSelector
} from 'Selector/expensesCategoriesFilter';
import { openDayDetails, setDayDetailsDate } from 'Action/dayDetails';
import { EXPENSES_BAR_CHART } from 'Constant';
import { filteredIncomeForTimeBarChart } from 'Selector/data/income';
import { checkedIncomeCategoriesWithColorSelector } from 'Selector/incomeCategoriesFilter';
import { periodFilterEndDateSelector, periodFilterStartDateSelector } from 'Selector/periodFilter';


const mapStateToProps = (state, { type }) => {
    const isExpensesChart = type === EXPENSES_BAR_CHART;
    const dataSelector = isExpensesChart ?
        filteredExpensesForTimeBarChart :
        filteredIncomeForTimeBarChart;
    const checkedCategiesSelector = isExpensesChart ?
        checkedExpensesCategoriesWithColorSelector :
        checkedIncomeCategoriesWithColorSelector;

    return {
        data: dataSelector(state),
        checkedCategories: checkedCategiesSelector(state),
        periodFilterStartDate: periodFilterStartDateSelector(state),
        periodFilterEndDate: periodFilterEndDateSelector(state),
    };
};
const mapDispatchToProps = {
    openDayDetails,
    setDayDetailsDate,
};
const connectTimeBarChart = superHoc(TimeBarChart, { mapStateToProps, mapDispatchToProps });


export default connectTimeBarChart;
