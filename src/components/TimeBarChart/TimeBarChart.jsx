import React, { Fragment, Component } from 'react';
import moment from 'moment';
import {
    BarChart,
    ReferenceLine,
    Bar,
    ResponsiveContainer,
    XAxis,
    YAxis,
    CartesianGrid,
} from 'recharts';

import { priceFormat } from 'Util';
import { Container, AverageDayAmount } from './TimeBarChart.styled';
import { DISPLAY_MOMENT_DATE_FORMAT, EXPENSES_BAR_CHART } from 'Constant';


class TimeBarChart extends Component {
    constructor(props) {
        super(props);

        this.openDayDetails = this.openDayDetails.bind(this);
    }

    openDayDetails(params) {
        const { type } = this.props;

        if (type !== EXPENSES_BAR_CHART) return;

        const { openDayDetails, setDayDetailsDate } = this.props;
        const { payload } = params;

        setDayDetailsDate(payload.date);
        openDayDetails();
    }

    render() {
        const {
            data,
            checkedCategories,
            periodFilterStartDate,
            periodFilterEndDate,
        } = this.props;

        const start = moment(periodFilterStartDate);
        const end = moment(periodFilterEndDate);
        const days = end.diff(start, 'days') + 1;

        const dataWithTotal = data.map((day) => {
            const dayTotal = Object.keys(day)
                .filter(category => category !== 'date')
                .reduce((acc, cur) => acc + day[cur], 0);

            return {
                ...day,
                total: dayTotal,
            };
        });
        const average = dataWithTotal.reduce((acc, cur) => acc + cur.total, 0) / days;
        const formatter = val => moment(val).format(DISPLAY_MOMENT_DATE_FORMAT);

        if (!data.length) return (
            <pre>No data.</pre>
        );

        return (
            <Fragment>
                {
                    average &&
                    <AverageDayAmount>
                        Average Day: { priceFormat(Math.round(average)) }
                    </AverageDayAmount>
                }
                <Container>
                    <ResponsiveContainer>
                        <BarChart
                            data={dataWithTotal}
                            margin={{
                                top: 20,
                                right: 0,
                                left: 0,
                                bottom: 20,
                            }}
                        >
                            <CartesianGrid strokeDasharray="3 3" />
                            <XAxis dataKey="date" tickFormatter={formatter} />
                            <YAxis />
                            {
                                checkedCategories.map(({ category, color }) => (
                                    <Bar
                                        dataKey={category}
                                        key={category}
                                        stackId="a"
                                        fill={color}
                                        onClick={this.openDayDetails}
                                    />
                                ))
                            }
                            <ReferenceLine
                                y={average}
                                stroke="red"
                                strokeDasharray="3 3"
                            />
                        </BarChart>
                    </ResponsiveContainer>
                </Container>
            </Fragment>
        );
    }
}


export default TimeBarChart;
