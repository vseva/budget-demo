import styled from 'styled-components';


export const Container = styled.div`
  height: 300px;
`;

export const AverageDayAmount = styled.h2`
  margin-bottom: 0;
`;
