import React from 'react';
import ReactModal from 'react-modal';

import CategoryExpensesTable from 'Component/CategoryExpensesTable';


const CategoryDetailsModal = ({
    categoryDetailsOpened,
    closeCategoryDetails,
}) => {
    if (!categoryDetailsOpened) return null;

    return (
        <ReactModal
            isOpen={categoryDetailsOpened}
            contentLabel="Category Expenses Details"
            onRequestClose={closeCategoryDetails}
        >
            <CategoryExpensesTable />
        </ReactModal>
    )
};


export default CategoryDetailsModal;
