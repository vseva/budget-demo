import superHoc from 'Component/superHoc';
import CategoryDetailsModal from './CategoryDetailsModal';
import { closeCategoryDetails } from 'Action/categoryDetails';
import { categoryDetailsOpenedSelector } from 'Selector/categoryDetails';


const mapStateToProps = state => ({
    categoryDetailsOpened: categoryDetailsOpenedSelector(state),
});
const mapDispatchToProps = {
    closeCategoryDetails,
};
const connectCategoryDetailsModal = superHoc(CategoryDetailsModal, { mapStateToProps, mapDispatchToProps });


export default connectCategoryDetailsModal;
