import React, { Fragment } from 'react';
import orderBy from 'lodash/orderBy';
import groupBy from 'lodash/groupBy';
import numeral from 'numeral';
import moment from 'moment';

import { COLORS, DISPLAY_MOMENT_DATE_FORMAT, EXPENSES_CATEGORIES_FILTER, HUMAN_MOMENT_DATE_FORMAT } from 'Constant';


export const getApiUrl = location => `${API_URL}${location}`;

export const groupExpensesData = (rows) => {
    const groupedData = groupBy(rows, 'category');

    const categoriesData = Object.keys(groupedData).reduce((acc, category, idx) => {
        const categoryItems = groupedData[category];
        const categoryAmount = categoryItems
            .reduce((accCategory, item) => accCategory += item.amount, 0);

        return acc.concat({
            category,
            categoryAmount,
            categoryItems,
            checked: true,
            color: getCategoryColor(category),
        });
    }, []);

    return orderBy(categoriesData, 'categoryAmount', 'desc');
};

export const groupIncomeData = (rows) => {
    const groupedData = groupBy(rows, 'category');

    const categoriesData = Object.keys(groupedData).reduce((acc, category, idx) => {
        const categoryItems = groupedData[category];
        const categoryAmount = categoryItems.reduce((accCategory, item) => accCategory += item.amount, 0);

        return acc.concat({
            category,
            categoryAmount,
            categoryItems,
            checked: true,
            color: getCategoryColor(category),
        });
    }, []);

    return orderBy(categoriesData, 'categoryAmount', 'desc');
};

export const priceFormat = (val, options = { withCurrency: true }) => {
    const { withCurrency, textString } = options;
    const formattedNumber = numeral(val).format('0,0');

    if (textString) return `${formattedNumber} ₽`;

    return withCurrency ?
        <Fragment>
            <span>{formattedNumber}</span>&nbsp;<span>₽</span>
        </Fragment>:
        formattedNumber;
};

export const getCategoriesFilterTitle = (filterType) => {
    if (filterType === EXPENSES_CATEGORIES_FILTER) return 'Expenses';

    return 'Income';
};

export const getCategoryColor = (category) => {
    const color = COLORS.find(color => color.label === category);

    return (color && color.value) || 'pink';
};

export const displayHumanDateFormat = (dateStr) => moment(dateStr).format(DISPLAY_MOMENT_DATE_FORMAT);
export const humanDateFormat = (dateStr) => moment(dateStr).format(HUMAN_MOMENT_DATE_FORMAT);