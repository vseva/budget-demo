import styled from 'styled-components';


const buttonProps = {
    width: ({ block }) => (block ? '100%' : 'auto'),
    opacity: ({ disabled }) => (disabled ? 0.7 : 1 ),
    cursor: ({ disabled }) => (disabled ? 'progress' : 'pointer' )
};

const Button = styled.button`
  background-color: #5b39ff;
  border: 0;
  cursor: ${buttonProps.cursor};
  opacity: ${buttonProps.opacity};
  color: #fff;
  padding: 10px;
  border-radius: 3px;
  text-transform: uppercase;
  font-size: 1em;
  width: ${buttonProps.width};
  outline: none;
`;


export default Button;
