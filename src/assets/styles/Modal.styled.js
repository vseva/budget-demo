import styled from 'styled-components';


export const ModalHeader = styled.div`
  margin-bottom: 10px;
  
  &:after {
    content: '';
    clear: both;
    display: table;
  }
`;

export const ModalHeaderTitle = styled.div`
  font-weight: bold;
  float: left;
  padding-right: 30px;
  padding-top: 7px;
`;

export const ModalCloseButtonWrap = styled.div`
  float: right;
  cursor: pointer;
  padding: 4px;
  width: 30px;
  height: 30px;
  
  svg {
    height: 100%;
    width: 100%;
  }
`;
