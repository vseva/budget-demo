#!/usr/bin/env bash

git pull
npm run build
pm2 delete index
NODE_ENV=production pm2 start api/index.js
