const path = require('path');
const fs = require('fs');
const orderBy = require('lodash/orderBy');
const get = require('lodash/get');


const parseExpensesData = (rows) => orderBy(rows.map((row, idx) => ({
    date: row[0],
    amount: row[1],
    description: row[2],
    vendor: row[3],
    category: row[4],
    rowIdx: idx + 2,
})), 'date');

const parseIncomeData = (rows) => orderBy(rows.map((row, idx) => ({
    date: row[0],
    amount: row[1],
    description: row[2],
    category: row[3],
    rowIdx: idx + 2,
})), 'date');

const prepareExpenseData = (data) => [
    data.date,
    data.amount,
    data.description,
    data.vendor,
    data.category,
];

const prepareIncomeData = (data) => [
    data.date,
    data.amount,
    data.description,
    data.category,
];

const jsoner = json => JSON.stringify(json, null, 3);

const getData = (sheets, auth) => new Promise((resolve, reject) => {
    sheets.spreadsheets.values.batchGet(
        {
            auth: auth,
            spreadsheetId: '1oFs6Qf4_75rKYpILEIkjSRJkS0vUGdfCtuWoJGCpUbc',
            ranges: [
                'Expenses!A2:E',
                'Income!A2:D',
            ],
            valueRenderOption: 'UNFORMATTED_VALUE',
            dateTimeRenderOption: 'FORMATTED_STRING',
        },
        (error, response) => {
            if (error) {
                console.error('getData inner error', error);

                const errorData = get(error, 'response.data.error', {});

                return reject({
                    error: {
                        message: errorData.message,
                        code: errorData.code,
                        errors: jsoner(errorData.errors),
                    }
                });
            }

            if (!response) {
                return reject({
                    error: {
                        message: 'No response found.',
                    }
                });
            }

            const rows = get(response, 'data.valueRanges', []);

            if (!rows.length) {
                return reject({
                    error: {
                        message: 'No data found.',
                    }
                });
            }

            resolve(rows);
        }
    );
});


const getCredentials = (production) => {
    if (production) {
        const privateKey = fs.readFileSync('/etc/letsencrypt/live/denlap.sevadenisov.com/privkey.pem', 'utf8');
        const certificate = fs.readFileSync('/etc/letsencrypt/live/denlap.sevadenisov.com/fullchain.pem', 'utf8');
        const ca = fs.readFileSync('/etc/letsencrypt/live/denlap.sevadenisov.com/fullchain.pem', 'utf8');

        return {
            key: privateKey,
            cert: certificate,
            ca: ca
        };
    }

    return {
        key: fs.readFileSync(path.join(__dirname, 'server.key')),
        cert: fs.readFileSync(path.join(__dirname, 'server.cert')),
    };
};


module.exports = {
    parseExpensesData,
    parseIncomeData,
    prepareExpenseData,
    prepareIncomeData,
    jsoner,
    getCredentials,
    getData,
};
