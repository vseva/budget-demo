'use strict';


const bodyParser = require('body-parser');
const path = require('path');
const fs = require('fs');
const https = require('https');
const { google } = require('googleapis');
const express = require('express');
const get = require('lodash/get');

const {
    parseExpensesData,
    parseIncomeData,
    prepareExpenseData,
    prepareIncomeData,
    jsoner,
    getData,
    getCredentials,
} = require('./helpers');


/* FIXME COPYPASTE */
const EXPENSE_ENTRY_TYPE = 'EXPENSE_ENTRY_TYPE';
const INCOME_ENTRY_TYPE = 'INCOME_ENTRY_TYPE';
/* /FIXME COPYPASTE */

const production = process.env.NODE_ENV === 'production';
const keys = JSON.parse(fs.readFileSync(path.join(__dirname, 'credentials.json')));
const SCOPES = ['https://www.googleapis.com/auth/spreadsheets'];
const TOKEN_PATH = `${__dirname}/token.json`;



const oAuth2Client = new google.auth.OAuth2(
    keys.web.client_id,
    keys.web.client_secret,
    keys.web.redirect_uris[production ? 0 : 1]
);

const app = express();
const sheets = google.sheets('v4');

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

app.get('/verify', (req, res) => {
    const code = req.query.code;

    oAuth2Client.getToken(code, (error, token) => {
        if (error) {
            const errorData = error.response.data;

            console.error('/verify getToken error', errorData);

            return res.status(400).send({
                error: {
                    message: errorData.error,
                }
            });
        }

        oAuth2Client.setCredentials(token);

        fs.writeFile(TOKEN_PATH, jsoner(token), (err) => {
            if (err) {
                return console.error('Token saving error: ', err);
            }

            console.log('Token stored to', TOKEN_PATH);
        });

        return res.send(jsoner({ success: true }));
    });
});

app.get('/access', (req, res) => {
    const authorizeUrl = oAuth2Client.generateAuthUrl({
        access_type: 'online',
        scope: SCOPES,
    });
    res.send(jsoner({ url: authorizeUrl }));
});

app.get('/pull', (req, res) => {
    fs.readFile(TOKEN_PATH, (err, token) => {
        if (err) return console.error('Pull token file read error');

        oAuth2Client.setCredentials(JSON.parse(token));

        getData(sheets, oAuth2Client)
            .then(response => {
                const expenses = parseExpensesData(response[0].values)
                    .filter(exp => exp.amount);
                const income = parseIncomeData(response[1].values)
                    .filter(inc => inc.amount);

                let responseData = {
                    expenses,
                    income,
                };

                return res.send(jsoner(responseData));
            })
            .catch(error => {
                console.error('getData catch fired', error);

                return res.status(400).send(error);
            });
    });
});

app.post('/add', (req, res) => {
    const { rowData, entryType } = req.body;
    const range = entryType === EXPENSE_ENTRY_TYPE ?
        'Expenses' :
        'Income';
    const values = entryType === EXPENSE_ENTRY_TYPE ?
        [prepareExpenseData(rowData)] :
        [prepareIncomeData(rowData)];

    fs.readFile(TOKEN_PATH, (err, token) => {
        if (err) return console.error('Pull token file read error');

        oAuth2Client.setCredentials(JSON.parse(token));

        sheets.spreadsheets.values.append({
            spreadsheetId: 'HERE_SPREADSHEET_ID',
            valueInputOption: 'RAW',
            insertDataOption: 'INSERT_ROWS',
            includeValuesInResponse: true,
            auth: oAuth2Client,
            resource: {
                values,
            },
            range,
        }, (error, response) => {
            if (error) {
                return res.send(jsoner({ success: false, error }));
            }

            const range = get(response, 'data.updates.updatedRange');
            const rowIdx = Number(range.split(':')[1].match(/\d/g).join(''));

            return res.send(jsoner({
                success: true,
                entryType,
                rowIdx,
            }));
        });
    });
});

app.get('/recognize', (req, res) => {
    imageRecognition().then(({ text, lines }) => {
        res.send(jsoner({ text }));
    })
});

app.post('/edit', (req, res) => {
    const { rowData, rowIdx, entryType } = req.body;
    const sheet = entryType === EXPENSE_ENTRY_TYPE ?
        'Expenses' :
        'Income';
    const values = entryType === EXPENSE_ENTRY_TYPE ?
        [prepareExpenseData(rowData)] :
        [prepareIncomeData(rowData)];

    fs.readFile(TOKEN_PATH, (err, token) => {
        if (err) return console.error('Pull token file read error');

        oAuth2Client.setCredentials(JSON.parse(token));

        sheets.spreadsheets.values.update({
            spreadsheetId: '1oFs6Qf4_75rKYpILEIkjSRJkS0vUGdfCtuWoJGCpUbc',
            valueInputOption: 'RAW',
            range: `${sheet}!A${rowIdx}`,
            auth: oAuth2Client,
            resource: {
                values,
            },
        }, (error) => {
            if (error) {
                return res.send(JSON.stringify({ success: false, error }, null, 3));
            }

            return res.send(JSON.stringify({ success: true, rowIdx, entryType }, null, 3));
        });
    });
});


https
    .createServer(getCredentials(production), app)
    .listen(3000, () => {
        console.log('api started on port 3000');
    });
